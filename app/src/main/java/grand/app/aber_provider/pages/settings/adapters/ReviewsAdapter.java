package grand.app.aber_provider.pages.settings.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import grand.app.aber_provider.R;
import grand.app.aber_provider.databinding.ItemReviewBinding;
import grand.app.aber_provider.pages.settings.models.rates.RatesItem;
import grand.app.aber_provider.pages.settings.viewModels.ItemReviewViewModel;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MenuView> {
    List<RatesItem> ratesItemList;

    public ReviewsAdapter() {
        this.ratesItemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review,
                parent, false);
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        RatesItem menuModel = ratesItemList.get(position);
        ItemReviewViewModel itemMenuViewModel = new ItemReviewViewModel(menuModel);
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(List<RatesItem> dataList) {
        this.ratesItemList.clear();
        ratesItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return ratesItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemReviewBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemReviewViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
