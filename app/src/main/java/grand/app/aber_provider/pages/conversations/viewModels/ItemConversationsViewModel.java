package grand.app.aber_provider.pages.conversations.viewModels;

import androidx.databinding.Bindable;

import grand.app.aber_provider.base.BaseViewModel;
import grand.app.aber_provider.pages.conversations.models.ConversationsData;
import grand.app.aber_provider.utils.Constants;

public class ItemConversationsViewModel extends BaseViewModel {
    public ConversationsData conversationsData;

    public ItemConversationsViewModel(ConversationsData conversationsData) {
        this.conversationsData = conversationsData;

    }

    @Bindable
    public ConversationsData getConversationsData() {
        return conversationsData;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
