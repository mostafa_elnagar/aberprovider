package grand.app.aber_provider.pages.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import javax.inject.Inject;

import grand.app.aber_provider.PassingObject;
import grand.app.aber_provider.databinding.FragmentMyAccountSettingsBinding;
import grand.app.aber_provider.model.base.Mutable;
import grand.app.aber_provider.pages.appWallet.AppWalletFragment;
import grand.app.aber_provider.pages.packages.PackagesFragment;
import grand.app.aber_provider.pages.profile.EditProfileFragment;
import grand.app.aber_provider.pages.settings.viewModels.MyAccountSettingsViewModel;
import grand.app.aber_provider.R;
import grand.app.aber_provider.base.BaseFragment;
import grand.app.aber_provider.base.IApplicationComponent;
import grand.app.aber_provider.base.MyApplication;
import grand.app.aber_provider.utils.Constants;
import grand.app.aber_provider.utils.helper.AppHelper;
import grand.app.aber_provider.utils.helper.MovementHelper;
import grand.app.aber_provider.utils.resources.ResourceManager;

public class MyAccountSettingsFragment extends BaseFragment {

    @Inject
    MyAccountSettingsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMyAccountSettingsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_account_settings, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.ABOUT:
                    MovementHelper.startActivity(requireActivity(), AboutAppFragment.class.getName(), getResources().getString(R.string.about), null);
                    break;
                case Constants.TERMS:
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(Constants.TERMS), getResources().getString(R.string.terms), TermsFragment.class.getName(), null);
                    break;
                case Constants.COMPLAINTS:
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(((Mutable) o).message), ResourceManager.getString(R.string.tv_account_suggest), ContactUsFragment.class.getName(), null);
                    break;
                case Constants.CONTACT:
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(((Mutable) o).message), ResourceManager.getString(R.string.tv_account_contact), ContactUsFragment.class.getName(), null);
                    break;
                case Constants.SOCIAL:
                    MovementHelper.startActivity(requireActivity(), SocialMediaFragment.class.getName(), ResourceManager.getString(R.string.social_media), null);
                    break;
                case Constants.PROFILE:
                    MovementHelper.startActivity(requireActivity(), EditProfileFragment.class.getName(), ResourceManager.getString(R.string.profile), null);
                    break;
                case Constants.SUBSCRIPTION:
                    MovementHelper.startActivity(requireActivity(), PackagesFragment.class.getName(), getString(R.string.package_title), null);
                    break;
                case Constants.WALLET:
                    MovementHelper.startActivity(requireActivity(), AppWalletFragment.class.getName(), getString(R.string.my_wallet), null);
                    break;
                case Constants.REVIEWS:
                    MovementHelper.startActivity(requireActivity(), ReviewsFragment.class.getName(), getString(R.string.my_reviews), null);
                    break;
                case Constants.RATE_APP:
                    AppHelper.rateApp(requireActivity());
                    break;
                case Constants.SHARE_BAR:
                    AppHelper.shareApp(requireActivity());
                    break;
                case Constants.SHOW_LOGOUT_WARNING:
                    getActivityBase().exitDialog(getString(R.string.logout_warning));
                    break;

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

}
