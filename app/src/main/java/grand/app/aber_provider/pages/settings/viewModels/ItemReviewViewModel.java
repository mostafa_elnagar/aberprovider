package grand.app.aber_provider.pages.settings.viewModels;


import androidx.databinding.Bindable;

import grand.app.aber_provider.base.BaseViewModel;
import grand.app.aber_provider.pages.settings.models.rates.RatesItem;


public class ItemReviewViewModel extends BaseViewModel {
    public RatesItem ratesItem;

    public ItemReviewViewModel(RatesItem ratesItem) {
        this.ratesItem = ratesItem;
    }

    @Bindable
    public RatesItem getRatesItem() {
        return ratesItem;
    }


}
