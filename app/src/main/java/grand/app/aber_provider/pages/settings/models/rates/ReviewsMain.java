package grand.app.aber_provider.pages.settings.models.rates;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReviewsMain {
    @SerializedName("app_rate")
    private float appRate;
    @SerializedName("avg_rate")
    private float avgRate;
    @SerializedName("users_review")
    private List<RatesItem> ratesItem;

    public float getAppRate() {
        return appRate;
    }

    public float getAvgRate() {
        return avgRate;
    }

    public List<RatesItem> getRatesItem() {
        return ratesItem;
    }
}
