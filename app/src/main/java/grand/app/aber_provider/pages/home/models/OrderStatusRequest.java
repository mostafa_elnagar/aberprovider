package grand.app.aber_provider.pages.home.models;

import com.google.gson.annotations.SerializedName;

public class OrderStatusRequest {
    @SerializedName("order_service_id")
    int orderId;
    @SerializedName("status")
    String status;
    @SerializedName("code")
    String code;
    @SerializedName("reason")
    String reason;


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
