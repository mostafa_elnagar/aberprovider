package grand.app.aber_provider.pages.home.models;

import com.google.gson.annotations.SerializedName;

public class ChangeOnlineRequest {
    @SerializedName("online")
    private int online;

    public ChangeOnlineRequest(int online) {
        this.online = online;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }
}
