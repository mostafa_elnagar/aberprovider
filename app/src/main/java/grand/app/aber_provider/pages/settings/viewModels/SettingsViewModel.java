package grand.app.aber_provider.pages.settings.viewModels;

import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import grand.app.aber_provider.R;
import grand.app.aber_provider.base.BaseViewModel;
import grand.app.aber_provider.base.MyApplication;
import grand.app.aber_provider.model.base.Mutable;
import grand.app.aber_provider.pages.settings.adapters.ContactsAdapter;
import grand.app.aber_provider.pages.settings.adapters.ReviewsAdapter;
import grand.app.aber_provider.pages.settings.adapters.SocialAdapter;
import grand.app.aber_provider.pages.settings.models.AboutData;
import grand.app.aber_provider.pages.settings.models.ContactUsRequest;
import grand.app.aber_provider.pages.settings.models.rates.ReviewsMain;
import grand.app.aber_provider.repository.SettingsRepository;
import grand.app.aber_provider.utils.Constants;
import grand.app.aber_provider.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class SettingsViewModel extends BaseViewModel {

    ContactUsRequest contactUsRequest;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository repository;
    AboutData aboutData;
    ContactsAdapter contactsAdapter;
    SocialAdapter socialAdapter;
    ReviewsAdapter reviewsAdapter;
    ReviewsMain reviewsMain;

    @Inject
    public SettingsViewModel(SettingsRepository repository) {
        aboutData = new AboutData();
        contactUsRequest = new ContactUsRequest();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void sendContact() {
        if (getContactUsRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(repository.sendContact(getContactUsRequest()));
        } else
            liveData.setValue(new Mutable(Constants.ERROR_TOAST));
    }

    public void getContact() {
        compositeDisposable.add(repository.getContact());
    }

    public void about() {
        compositeDisposable.add(repository.about());
    }

    public void providerReviews() {
        compositeDisposable.add(repository.reviews());
    }

    public void terms() {
        if (getPassingObject().getObject().equals(Constants.TERMS))
            compositeDisposable.add(repository.terms());
        else
            compositeDisposable.add(repository.privacy());
    }

    public void socialMedia() {
        compositeDisposable.add(repository.getSocial());
    }

    public void onLangChange(RadioGroup radioGroup, int id) {
        if (id == R.id.arabic) {
            lang = "ar";
        } else if (id == R.id.english)
            lang = "en";
        else
            lang = "ur";
    }

    public void changeLang() {
        liveData.setValue(new Mutable(Constants.LANGUAGE));
    }

    @Bindable
    public ContactsAdapter getContactsAdapter() {
        return this.contactsAdapter == null ? this.contactsAdapter = new ContactsAdapter() : this.contactsAdapter;
    }

    @Bindable
    public SocialAdapter getSocialAdapter() {
        return this.socialAdapter == null ? this.socialAdapter = new SocialAdapter() : this.socialAdapter;
    }

    @Bindable
    public ReviewsAdapter getReviewsAdapter() {
        return this.reviewsAdapter == null ? this.reviewsAdapter = new ReviewsAdapter() : this.reviewsAdapter;
    }

    @Bindable
    public AboutData getAboutData() {
        return aboutData;
    }

    @Bindable
    public void setAboutData(AboutData aboutData) {
        notifyChange(BR.aboutData);
        this.aboutData = aboutData;
    }

    @Bindable
    public ReviewsMain getReviewsMain() {
        return this.reviewsMain == null ? this.reviewsMain = new ReviewsMain() : this.reviewsMain;
    }

    @Bindable
    public void setReviewsMain(ReviewsMain reviewsMain) {
        userData.setAvgRate(String.valueOf(reviewsMain.getAvgRate()));
        UserHelper.getInstance(MyApplication.getInstance()).userLogin(userData);
        getReviewsAdapter().update(reviewsMain.getRatesItem());
        notifyChange(BR.reviewsAdapter);
        this.reviewsMain = reviewsMain;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public SettingsRepository getRepository() {
        return repository;
    }


    public ContactUsRequest getContactUsRequest() {
        return contactUsRequest;
    }

}
