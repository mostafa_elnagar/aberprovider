package grand.app.aber_provider.pages.settings.models.rates;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_provider.model.base.StatusMessage;

public class ReviewsResponse extends StatusMessage {
    @SerializedName("data")
    private ReviewsMain reviewsMain;

    public ReviewsMain getReviewsMain() {
        return reviewsMain;
    }
}
