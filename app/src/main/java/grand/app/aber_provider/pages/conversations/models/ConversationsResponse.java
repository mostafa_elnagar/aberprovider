package grand.app.aber_provider.pages.conversations.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.aber_provider.model.base.StatusMessage;


public class ConversationsResponse extends StatusMessage {
    @SerializedName("data")
    private ConversationsMain conversationsMain;

    public ConversationsMain getConversationsMain() {
        return conversationsMain;
    }
}