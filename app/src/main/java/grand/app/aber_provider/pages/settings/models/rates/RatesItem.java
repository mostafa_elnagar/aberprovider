package grand.app.aber_provider.pages.settings.models.rates;

import com.google.gson.annotations.SerializedName;

import grand.app.aber_provider.pages.auth.models.UserData;

public class RatesItem {
    @SerializedName("rate")
    private String rate;
    @SerializedName("date")
    private String createdAt;
    @SerializedName("review")
    private String comment;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;
    @SerializedName("user")
    private UserData user;
    @SerializedName("id")
    private int id;


    public String getRate() {
        return rate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getComment() {
        return comment;
    }

    public int getId() {
        return id;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UserData getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
