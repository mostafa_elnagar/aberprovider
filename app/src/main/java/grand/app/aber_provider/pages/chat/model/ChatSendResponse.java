package grand.app.aber_provider.pages.chat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.aber_provider.model.base.StatusMessage;
import grand.app.aber_provider.pages.conversations.models.ConversationsData;

public class ChatSendResponse extends StatusMessage {

    @SerializedName("data")
    @Expose
    private ConversationsData data;

    public ConversationsData getData() {
        return data;
    }
}
