package grand.app.aber_provider.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import grand.app.aber_provider.connection.ConnectionHelper;
import grand.app.aber_provider.model.base.Mutable;
import grand.app.aber_provider.model.base.StatusMessage;
import grand.app.aber_provider.pages.home.models.ChangeOnlineRequest;
import grand.app.aber_provider.pages.home.models.HomeResponse;
import grand.app.aber_provider.pages.home.models.OrderStatusRequest;
import grand.app.aber_provider.pages.myOrders.models.MyOrdersResponse;
import grand.app.aber_provider.pages.packages.models.PackagesResponse;
import grand.app.aber_provider.pages.packages.models.RequestToSend;
import grand.app.aber_provider.pages.orderDetails.models.OrderDetailsResponse;
import grand.app.aber_provider.utils.Constants;
import grand.app.aber_provider.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class ServicesRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public ServicesRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getPackages() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PACKAGES, new Object(), PackagesResponse.class,
                Constants.PACKAGES, true);
    }

    public Disposable subscribe(int id) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SUBSCRIBE, new RequestToSend(id), StatusMessage.class,
                Constants.SUBSCRIPTION, true);
    }

    public Disposable getHome() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.HOME, new Object(), HomeResponse.class,
                Constants.HOME, true);
    }


    public Disposable myOrders(int page, boolean showProgress, int orderType) {
        if (orderType == Constants.CURRENT)
            return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MY_CURRENT_ORDERS + page, new Object(), MyOrdersResponse.class,
                    Constants.MY_ORDERS, showProgress);
        else
            return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MY_LAST_ORDERS + page, new Object(), MyOrdersResponse.class,
                    Constants.MY_ORDERS, showProgress);
    }


    public Disposable orderDetails(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.POST_DETAILS + orderId, new Object(), OrderDetailsResponse.class,
                Constants.ORDER_DETAILS, true);
    }

    public Disposable changeStatus(OrderStatusRequest orderStatusRequest) {
        if (orderStatusRequest.getStatus().equals("-1")) {
            orderStatusRequest.setStatus(null);
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.REJECT_ORDER, orderStatusRequest, StatusMessage.class,
                    Constants.REJECT_ORDER, true);
        } else
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_STATUS, orderStatusRequest, StatusMessage.class,
                    Constants.CHANGE_ORDER_STATUS, true);

    }

    public Disposable search(int page, boolean showProgress, String search, String type) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SEARCH_POSTS + type + "&search=" + search + "&page=" + page, new Object(), HomeResponse.class,
                Constants.SEARCH, showProgress);
    }

    public Disposable changeOnlineStatus(int online) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_ONLINE_STATUS, new ChangeOnlineRequest(online), StatusMessage.class,
                Constants.CHANGE_ONLINE_STATUS, true);
    }

}