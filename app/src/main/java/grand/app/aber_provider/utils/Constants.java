package grand.app.aber_provider.utils;

public class Constants {

    public static final String MESSAGE = "message";
    public static final String FOLLOW_ORDER = "FOLLOW_ORDER";
    public static final String COMMERCIAL_IMAGE = "commercial_register_photo";
    public final static String TAX_CARD_PHOTO = "tax_card_photo";
    public final static int COMMERCIAL_REQUEST = 6001;
    public final static int TAX_REQUEST = 6002;
    public final static int IDENTITY_REQUEST = 6003;
    public final static int PASSPORT_REQUEST = 6004;
    public static final String CITIES = "CITIES";
    public static final String COUNTRIES = "COUNTRIES";

    public final static int IDENTITY_BACK_REQUEST = 6005;
    public final static int LICENSE_REQUEST = 6006;
    public final static int LICENSE_BACK_REQUEST = 6007;
    public final static String CONFIRM_CODE = "CONFIRM_CODE";
    public final static String NOT_VERIFIED = "NOT_VERIFIED";
    public final static String PACKAGES = "PACKAGES";
    public final static String EMAIL = "email";
    public final static String FIELD = "FIELD";
    public final static String NOT_MATCH_PASSWORD = "NOT_MATCH_PASSWORD";
    public static final String GET_CONTACT = "GET_CONTACT";
    public static final int CURRENT = 0;
    public static final int LAST = 1;
    public static final String BACK = "BACK";

    public final static String UPDATE_PROFILE = "update_profile";


    public final static String IMAGE = "image";
    public final static String MY_ORDERS = "MY_ORDERS";
    public final static String HOME = "HOME";
    public final static String BACKGROUND_API = "BACKGROUND_API";

    public final static String LOGOUT = "logout";
    public static final String LOGOUT_API = "LOGOUT_API";
    public final static String NATIONAL_ID_PHOTO = "front_national_id";

    public final static String SHOW_LOGOUT_WARNING = "SHOW_LOGOUT_WARNING";
    public final static String PASSPORT_PHOTO = "passport_photo";
    public final static String SEND_MESSAGE = "SEND_MESSAGE";

    public final static String ERROR_TOAST = "error_toast";

    public final static String ERROR = "error";
    public final static String SHOW_PROGRESS = "showProgress";
    public final static String HIDE_PROGRESS = "hideProgress";
    public final static String SERVER_ERROR = "serverError";
    public final static String ERROR_NOT_FOUND = "not_found";
    public final static String RESPONSE_URL_CHANGED = "changed";

    public final static String FAILURE_CONNECTION = "failure_connection";
    public final static String ORDER_DETAILS = "ORDER_DETAILS";


    public final static String LANGUAGE = "language";
    public static final String DEFAULT_LANGUAGE = "ar";
    public static final String MENU_ACCOUNT = "MENU_ACCOUNT";
    public static final String REVIEWS = "REVIEWS";
    public static final int LOCATION_REQUEST = 6005;
    public static final String PAGE = "page";
    public final static String NAME_BAR = "name_bar";
    public static final String BUNDLE = "bundle";
    public static final String LICENSE_IMAGE = "front_driving_license";
    public static final String LICENSE_BACK_IMAGE = "back_driving_license";
    public static final String NATIONAL_BACK_ID_PHOTO = "back_national_id";
    public static final String REJECT_ORDER = "REJECT_ORDER";
    public static final String CHANGE_ORDER_STATUS = "CHANGE_ORDER_STATUS";
    public static final String CALL = "CALL";
    public static final String GET_USER_DOCUMENTS = "GET_USER_DOCUMENTS";
    public static final String SHARE_BAR = "SHARE_BAR";
    public static final String PROFILE = "PROFILE";


    //REQUESTS
    public final static int POST_REQUEST = 200;
    public final static int GET_REQUEST = 201;
    public final static int DELETE_REQUEST = 202;

    //RESPONSES
    public static final int RESPONSE_SUCCESS = 200;
    public static final int RESPONSE_JWT_EXPIRE = 403;
    public static final int RESPONSE_404 = 404;
    public static final int RESPONSE_CHANGE = 300;
    public final static int AUTOCOMPLETE_REQUEST_CODE = 203;
    public final static int FILE_TYPE_IMAGE = 378;
    public final static int FILE_TYPE_VIDEO = 379;
    public final static int CHECK_CONFIRM_NAV_REGISTER = 0;
    public final static int CHECK_CONFIRM_NAV_FORGET = 1;

    public static final String front_car_image = "front_car_image";
    public static final int ORDER_DETAILS_REQUEST = 380;

    public static final String back_car_image = "back_car_image";

    public static final String PRIVACY = "PRIVACY";
    public static final String CHAT = "CHAT";
    public static final int NOT_DONE = 6;
    public static final int DONE = 7;

    public static final String license_image = "license_image";
    public static final String SEARCH_LOCATION = "SEARCH_LOCATION";
    public static final String ABOUT = "about";
    public static final String TERMS = "terms";
    public static final String LOGIN = "login";
    public static final String LAT = "LAT";
    public static final String LNG = "LNG";
    public static final String FORGET_PASSWORD = "forget_password";
    public static final String QUESTIONS = "QUESTIONS";
    public static final int CUSTOM_REQUEST_CODE = 532;
    public static final String REGISTER = "register";
    public static final String SEARCH = "SEARCH";
    public static final int RESPONSE_405 = 405;
    public static boolean DATA_CHANGED = false;
    public static final String MORE = "MORE";
    public static final String RATE_APP = "RATE_APP";
    public static final String SOCIAL = "SOCIAL";
    public static final String WALLET = "WALLET";
    public static final String PICK_UP_LOCATION = "PICK_UP_LOCATION";
    public static final String CONTACT = "CONTACT";
    public static final String COMPLAINTS = "COMPLAINTS";
    public static final String CITY = "CITY";
    public static final String ADDRESS = "ADDRESS";
    public static final String NOTIFICATIONS = "notifications";
    public static final String PICKED_SUCCESSFULLY = "PICKED_SUCCESSFULLY";
    public static final String CHANGE_PASSWORD = "forget-password";
    public static final String SERVICES = "services";
    public static final String SUBSCRIPTION = "SUBSCRIPTION";
    public static final String MENU_HOME = "menu_home";
    public static final String NEXT = "next";
    public static final String PREVIOUS = "PREVIOUS";
    public static final String START_APP = "START_APP";
    public static final String BOARD = "board";
    public static final String MENu = "menu";
    // NOTIFICATIONS
    public static final String TYPE = "type";
    public static final String ORDER_ID = "order_service_id";
    public static final String ORDER_SERVICE = "order_service";
    //NOTIFICATIONS TYPES
    public static final String TIER = "tyre";
    public static final String OIL = "oil";
    public static final String HIDDEN = "glass";
    public static final String FUEL = "fuel";
    public static final String OPEN_CAR = "vehicle";
    public static final String BATTERY = "battery";
    public static final String CONVERSATIONS = "CONVERSATIONS";
    public static final String CHANGE_ONLINE_STATUS = "CHANGE_ONLINE_STATUS";


}

