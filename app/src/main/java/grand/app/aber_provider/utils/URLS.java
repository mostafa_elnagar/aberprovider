package grand.app.aber_provider.utils;

public class URLS {
    public final static String BASE_URL = "https://aber.my-staff.net/api/";
    // post
    public final static String PACKAGES = "v1/provider/packages";
    public final static String SUBSCRIBE = "v1/provider/package";
    public final static String HOME = "v1/provider/home";
    public final static String CONVERSATIONS = "v1.1/chats?page=";
    public final static String SERVICES = "services";
    public final static String LOGIN_SOCIAL = "v1/social_register";
    public final static String LOGIN_PASSWORD = "v1/provider/login";
    public final static String REGISTER = "v1.1/provider/register";
    public final static String COUNTRIES = "v1/countries";
    public final static String CITIES = "v1/cities?governorate_id=";
    public final static String BOARD = "v1/provider/welcome";
    public final static String CONFIRM_CODE = "v1/provider/code_check";
    public final static String UPDATE_PROFILE = "v1/provider/profile";
    public static final String FORGET_PASSWORD = "v1/provider/code_send";
    public static final String CHANGE_PASSWORD = "v1/provider/change_password";
    public final static String GET_CONTACT = "v1/contact_us";
    public final static String SEARCH_POSTS = "v1/search?type=";
    public final static String CHANGE_STATUS = "v1.1/provider/order/update_status";
    public final static String REJECT_ORDER = "v1/provider/order/reject";
    public final static String MY_CURRENT_ORDERS = "v1/provider/orders/current?page=";
    public final static String MY_LAST_ORDERS = "v1/provider/orders/latest?page=";
    public final static String POST_DETAILS = "v1/provider/order/";
    public final static String WALLET_HISTORY = "v1/provider/wallet";
    public static final String GET_SERVICES = "v1/provider/main_services?country_id=";
    public static final String CONTACT_US = "v1/contact_us";
    public static final String NOTIFICATIONS = "v1/provider/notifications?page=";
    public final static String GET_USER_DOCUMENTS = "delegate/delegate-documents";
    public final static String USER_PROFILE = "v1/provider/profile";
    public static final String GET_SOCIAL = "v1/social_media";
    public static final String PRIVACY = "v1/privacy_policy";
    public static final String CHANGE_PROFILE_PASSWORD = "v1/provider/profile/password";
    public static final String ABOUT = "v1/about_us";
    public static final String TERMS = "v1/terms_condition";
    public static final String CHAT = "v1.1/chat/messages?user_id=";
    public static final String SEND_MESSAGE = "v1.1/message";
    public static final String LOG_OUT = "v1/provider/logout";
    public static final String CHANGE_ONLINE_STATUS = "v1.1/provider/availability";
    public static final String REVIEWS = "v1.1/provider/profile/rate";
}
