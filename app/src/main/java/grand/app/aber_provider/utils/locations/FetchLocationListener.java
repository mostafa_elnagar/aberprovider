package grand.app.aber_provider.utils.locations;

import android.location.Location;

public interface FetchLocationListener {
    public void location(Location location);
}
