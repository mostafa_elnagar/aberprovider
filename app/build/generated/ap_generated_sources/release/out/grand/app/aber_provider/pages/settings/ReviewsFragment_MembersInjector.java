// Generated by Dagger (https://dagger.dev).
package grand.app.aber_provider.pages.settings;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_provider.pages.settings.viewModels.SettingsViewModel;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ReviewsFragment_MembersInjector implements MembersInjector<ReviewsFragment> {
  private final Provider<SettingsViewModel> viewModelProvider;

  public ReviewsFragment_MembersInjector(Provider<SettingsViewModel> viewModelProvider) {
    this.viewModelProvider = viewModelProvider;
  }

  public static MembersInjector<ReviewsFragment> create(
      Provider<SettingsViewModel> viewModelProvider) {
    return new ReviewsFragment_MembersInjector(viewModelProvider);
  }

  @Override
  public void injectMembers(ReviewsFragment instance) {
    injectViewModel(instance, viewModelProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_provider.pages.settings.ReviewsFragment.viewModel")
  public static void injectViewModel(ReviewsFragment instance, SettingsViewModel viewModel) {
    instance.viewModel = viewModel;
  }
}
