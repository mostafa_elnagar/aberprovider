package grand.app.aber_provider.databinding;
import grand.app.aber_provider.R;
import grand.app.aber_provider.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemReviewBindingImpl extends ItemReviewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.flow3, 6);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final grand.app.aber_provider.customViews.views.CustomTextViewRegular mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemReviewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemReviewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[4]
            , (androidx.constraintlayout.helper.widget.Flow) bindings[6]
            , (com.google.android.material.imageview.ShapeableImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[3]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[2]
            );
        this.createAt.setTag(null);
        this.icProviderRate.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView5 = (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[5];
        this.mboundView5.setTag(null);
        this.myRate.setTag(null);
        this.tvProviderName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemViewModel == variableId) {
            setItemViewModel((grand.app.aber_provider.pages.settings.viewModels.ItemReviewViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemViewModel(@Nullable grand.app.aber_provider.pages.settings.viewModels.ItemReviewViewModel ItemViewModel) {
        updateRegistration(0, ItemViewModel);
        this.mItemViewModel = ItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeItemViewModel((grand.app.aber_provider.pages.settings.viewModels.ItemReviewViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeItemViewModel(grand.app.aber_provider.pages.settings.viewModels.ItemReviewViewModel ItemViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.ratesItem) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_provider.pages.settings.viewModels.ItemReviewViewModel itemViewModel = mItemViewModel;
        java.lang.String itemViewModelRatesItemImage = null;
        grand.app.aber_provider.pages.settings.models.rates.RatesItem itemViewModelRatesItem = null;
        java.lang.String itemViewModelRatesItemName = null;
        java.lang.String itemViewModelRatesItemRate = null;
        java.lang.String itemViewModelRatesItemCreatedAt = null;
        java.lang.String itemViewModelRatesItemComment = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (itemViewModel != null) {
                    // read itemViewModel.ratesItem
                    itemViewModelRatesItem = itemViewModel.getRatesItem();
                }


                if (itemViewModelRatesItem != null) {
                    // read itemViewModel.ratesItem.image
                    itemViewModelRatesItemImage = itemViewModelRatesItem.getImage();
                    // read itemViewModel.ratesItem.name
                    itemViewModelRatesItemName = itemViewModelRatesItem.getName();
                    // read itemViewModel.ratesItem.rate
                    itemViewModelRatesItemRate = itemViewModelRatesItem.getRate();
                    // read itemViewModel.ratesItem.createdAt
                    itemViewModelRatesItemCreatedAt = itemViewModelRatesItem.getCreatedAt();
                    // read itemViewModel.ratesItem.comment
                    itemViewModelRatesItemComment = itemViewModelRatesItem.getComment();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.createAt, itemViewModelRatesItemCreatedAt);
            grand.app.aber_provider.base.ApplicationBinding.loadCommentImage(this.icProviderRate, itemViewModelRatesItemImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, itemViewModelRatesItemComment);
            grand.app.aber_provider.base.ApplicationBinding.setRate(this.myRate, itemViewModelRatesItemRate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProviderName, itemViewModelRatesItemName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemViewModel
        flag 1 (0x2L): itemViewModel.ratesItem
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}