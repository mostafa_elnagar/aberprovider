package grand.app.aber_provider.databinding;
import grand.app.aber_provider.R;
import grand.app.aber_provider.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRegisterDocumentsBindingImpl extends FragmentRegisterDocumentsBinding implements grand.app.aber_provider.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.register, 17);
        sViewsWithIds.put(R.id.progress_percentage, 18);
        sViewsWithIds.put(R.id.tv_percentage, 19);
        sViewsWithIds.put(R.id.card_personal_info, 20);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView10;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView12;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView14;
    @NonNull
    private final com.google.android.material.textfield.MaterialAutoCompleteTextView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView6;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView8;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback83;
    @Nullable
    private final android.view.View.OnClickListener mCallback89;
    @Nullable
    private final android.view.View.OnClickListener mCallback87;
    @Nullable
    private final android.view.View.OnClickListener mCallback86;
    @Nullable
    private final android.view.View.OnClickListener mCallback90;
    @Nullable
    private final android.view.View.OnClickListener mCallback88;
    @Nullable
    private final android.view.View.OnClickListener mCallback84;
    @Nullable
    private final android.view.View.OnClickListener mCallback85;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener autoandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.commercial_register
            //         is viewmodel.request.setCommercial_register((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(auto);
            // localize variables for thread safety
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel.request.commercial_register
            java.lang.String viewmodelRequestCommercialRegister = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setCommercial_register(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView10androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.passport_image
            //         is viewmodel.request.setPassport_image((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView10);
            // localize variables for thread safety
            // viewmodel.request.passport_image
            java.lang.String viewmodelRequestPassportImage = null;
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setPassport_image(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView12androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.license_image
            //         is viewmodel.request.setLicense_image((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView12);
            // localize variables for thread safety
            // viewmodel.request.license_image
            java.lang.String viewmodelRequestLicenseImage = null;
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setLicense_image(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView14androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.license_back_image
            //         is viewmodel.request.setLicense_back_image((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView14);
            // localize variables for thread safety
            // viewmodel.request.license_back_image
            java.lang.String viewmodelRequestLicenseBackImage = null;
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setLicense_back_image(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.tax_image
            //         is viewmodel.request.setTax_image((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request.tax_image
            java.lang.String viewmodelRequestTaxImage = null;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setTax_image(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView6androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.identity_image
            //         is viewmodel.request.setIdentity_image((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView6);
            // localize variables for thread safety
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel.request.identity_image
            java.lang.String viewmodelRequestIdentityImage = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setIdentity_image(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView8androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.request.identity_back_image
            //         is viewmodel.request.setIdentity_back_image((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView8);
            // localize variables for thread safety
            // viewmodel.request.identity_back_image
            java.lang.String viewmodelRequestIdentityBackImage = null;
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.request
            grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
            // viewmodel.request != null
            boolean viewmodelRequestJavaLangObjectNull = false;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelRequest = viewmodel.getRequest();

                viewmodelRequestJavaLangObjectNull = (viewmodelRequest) != (null);
                if (viewmodelRequestJavaLangObjectNull) {




                    viewmodelRequest.setIdentity_back_image(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentRegisterDocumentsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentRegisterDocumentsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 8
            , (androidx.appcompat.widget.AppCompatButton) bindings[15]
            , (com.google.android.material.textfield.MaterialAutoCompleteTextView) bindings[2]
            , (androidx.cardview.widget.CardView) bindings[20]
            , (com.google.android.material.textfield.TextInputLayout) bindings[3]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[7]
            , (com.google.android.material.textfield.TextInputLayout) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[16]
            , (com.google.android.material.progressindicator.LinearProgressIndicator) bindings[18]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[17]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[19]
            );
        this.appCompatButtonNext.setTag(null);
        this.auto.setTag(null);
        this.inputCompanyName.setTag(null);
        this.inputDriveBack.setTag(null);
        this.inputDriveFront.setTag(null);
        this.inputIdentityBack.setTag(null);
        this.inputIdentityFront.setTag(null);
        this.inputName.setTag(null);
        this.inputPassport.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView10 = (androidx.appcompat.widget.AppCompatEditText) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView12 = (androidx.appcompat.widget.AppCompatEditText) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView14 = (androidx.appcompat.widget.AppCompatEditText) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView4 = (com.google.android.material.textfield.MaterialAutoCompleteTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatEditText) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (androidx.appcompat.widget.AppCompatEditText) bindings[8];
        this.mboundView8.setTag(null);
        this.progress.setTag(null);
        setRootTag(root);
        // listeners
        mCallback83 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 1);
        mCallback89 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 7);
        mCallback87 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 5);
        mCallback86 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 4);
        mCallback90 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 8);
        mCallback88 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 6);
        mCallback84 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 2);
        mCallback85 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x400L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_provider.pages.auth.register.RegisterViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_provider.pages.auth.register.RegisterViewModel Viewmodel) {
        updateRegistration(4, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodelRequestIdentityBackError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewmodelRequestLicenseError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewmodelRequestCommercialError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewmodelRequestTaxError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewmodel((grand.app.aber_provider.pages.auth.register.RegisterViewModel) object, fieldId);
            case 5 :
                return onChangeViewmodelRequestPassportError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewmodelRequestLicenseBackError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 7 :
                return onChangeViewmodelRequestIdentityError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodelRequestIdentityBackError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestIdentityBackError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodelRequestLicenseError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestLicenseError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodelRequestCommercialError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestCommercialError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodelRequestTaxError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestTaxError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_provider.pages.auth.register.RegisterViewModel Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        else if (fieldId == BR.request) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        else if (fieldId == BR.message) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodelRequestPassportError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestPassportError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodelRequestLicenseBackError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestLicenseBackError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodelRequestIdentityError(androidx.databinding.ObservableField<java.lang.String> ViewmodelRequestIdentityError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewmodelRequestLicenseBackImage = null;
        boolean viewmodelRequestLicenseBackErrorJavaLangObjectNull = false;
        boolean viewmodelRequestLicenseBackErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        java.lang.String viewmodelRequestCommercialRegister = null;
        java.lang.String viewmodelRequestLicenseBackErrorGet = null;
        java.lang.String viewmodelRequestLicenseErrorGet = null;
        java.lang.String viewmodelMessage = null;
        java.lang.String viewmodelRequestIdentityBackImage = null;
        java.lang.String viewmodelRequestTaxImage = null;
        boolean textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESS = false;
        java.lang.String viewmodelRequestPassportImage = null;
        int userHelperGetInstanceContextAccountTypeEqualsJavaLangString0ViewGONEViewVISIBLE = 0;
        boolean textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = false;
        boolean viewmodelRequestTaxErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        java.lang.String viewmodelRequestTaxErrorGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestIdentityBackError = null;
        boolean viewmodelMessageEqualsConstantsSHOWPROGRESS = false;
        int textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        boolean viewmodelRequestIdentityErrorJavaLangObjectNull = false;
        boolean viewmodelRequestCommercialErrorJavaLangObjectNull = false;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestLicenseError = null;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestCommercialError = null;
        boolean viewmodelRequestIdentityBackErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        boolean viewmodelRequestCommercialErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestTaxError = null;
        grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;
        grand.app.aber_provider.pages.auth.models.RegisterRequest viewmodelRequest = null;
        boolean textUtilsIsEmptyViewmodelMessage = false;
        boolean TextUtilsIsEmptyViewmodelMessage1 = false;
        java.lang.String viewmodelRequestIdentityErrorGet = null;
        boolean viewmodelRequestTaxErrorJavaLangObjectNull = false;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestPassportError = null;
        boolean userHelperGetInstanceContextAccountTypeEqualsJavaLangString0 = false;
        boolean viewmodelRequestIdentityErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        boolean viewmodelMessageEqualsConstantsHIDEPROGRESS = false;
        int userHelperGetInstanceContextAccountTypeEqualsJavaLangString1ViewGONEViewVISIBLE = 0;
        java.lang.String viewmodelRequestCommercialErrorGet = null;
        android.graphics.drawable.Drawable textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESSAppCompatButtonNextAndroidDrawableCornerViewGradientAppCompatButtonNextAndroidDrawableCornerViewPrimaryMedium = null;
        boolean viewmodelRequestPassportErrorJavaLangObjectNull = false;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestLicenseBackError = null;
        java.lang.String viewmodelRequestIdentityImage = null;
        boolean viewmodelRequestIdentityBackErrorJavaLangObjectNull = false;
        java.lang.String viewmodelRequestLicenseImage = null;
        boolean viewmodelRequestLicenseErrorJavaLangObjectNull = false;
        boolean userHelperGetInstanceContextAccountTypeEqualsJavaLangString1 = false;
        java.lang.String userHelperGetInstanceContextAccountType = null;
        androidx.databinding.ObservableField<java.lang.String> viewmodelRequestIdentityError = null;
        boolean viewmodelRequestPassportErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        java.lang.String viewmodelRequestPassportErrorGet = null;
        boolean viewmodelRequestLicenseErrorJavaLangObjectNullBooleanTrueBooleanFalse = false;
        java.lang.String viewmodelRequestIdentityBackErrorGet = null;
        boolean textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;

        if ((dirtyFlags & 0x7ffL) != 0) {


            if ((dirtyFlags & 0x610L) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.message
                        viewmodelMessage = viewmodel.getMessage();
                    }


                    // read TextUtils.isEmpty(viewmodel.message)
                    textUtilsIsEmptyViewmodelMessage = android.text.TextUtils.isEmpty(viewmodelMessage);
                if((dirtyFlags & 0x610L) != 0) {
                    if(textUtilsIsEmptyViewmodelMessage) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }


                    // read !TextUtils.isEmpty(viewmodel.message)
                    TextUtilsIsEmptyViewmodelMessage1 = !textUtilsIsEmptyViewmodelMessage;
                if((dirtyFlags & 0x610L) != 0) {
                    if(TextUtilsIsEmptyViewmodelMessage1) {
                            dirtyFlags |= 0x4000000000L;
                    }
                    else {
                            dirtyFlags |= 0x2000000000L;
                    }
                }
            }
            if ((dirtyFlags & 0x5ffL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.request
                        viewmodelRequest = viewmodel.getRequest();
                    }

                if ((dirtyFlags & 0x510L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.license_back_image
                            viewmodelRequestLicenseBackImage = viewmodelRequest.getLicense_back_image();
                            // read viewmodel.request.commercial_register
                            viewmodelRequestCommercialRegister = viewmodelRequest.getCommercial_register();
                            // read viewmodel.request.identity_back_image
                            viewmodelRequestIdentityBackImage = viewmodelRequest.getIdentity_back_image();
                            // read viewmodel.request.tax_image
                            viewmodelRequestTaxImage = viewmodelRequest.getTax_image();
                            // read viewmodel.request.passport_image
                            viewmodelRequestPassportImage = viewmodelRequest.getPassport_image();
                            // read viewmodel.request.identity_image
                            viewmodelRequestIdentityImage = viewmodelRequest.getIdentity_image();
                            // read viewmodel.request.license_image
                            viewmodelRequestLicenseImage = viewmodelRequest.getLicense_image();
                        }
                }
                if ((dirtyFlags & 0x511L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.identityBackError
                            viewmodelRequestIdentityBackError = viewmodelRequest.identityBackError;
                        }
                        updateRegistration(0, viewmodelRequestIdentityBackError);


                        if (viewmodelRequestIdentityBackError != null) {
                            // read viewmodel.request.identityBackError.get()
                            viewmodelRequestIdentityBackErrorGet = viewmodelRequestIdentityBackError.get();
                        }


                        // read viewmodel.request.identityBackError.get() != null
                        viewmodelRequestIdentityBackErrorJavaLangObjectNull = (viewmodelRequestIdentityBackErrorGet) != (null);
                    if((dirtyFlags & 0x511L) != 0) {
                        if(viewmodelRequestIdentityBackErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x1000000L;
                        }
                        else {
                                dirtyFlags |= 0x800000L;
                        }
                    }


                        // read viewmodel.request.identityBackError.get() != null ? true : false
                        viewmodelRequestIdentityBackErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestIdentityBackErrorJavaLangObjectNull) ? (true) : (false));
                }
                if ((dirtyFlags & 0x512L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.licenseError
                            viewmodelRequestLicenseError = viewmodelRequest.licenseError;
                        }
                        updateRegistration(1, viewmodelRequestLicenseError);


                        if (viewmodelRequestLicenseError != null) {
                            // read viewmodel.request.licenseError.get()
                            viewmodelRequestLicenseErrorGet = viewmodelRequestLicenseError.get();
                        }


                        // read viewmodel.request.licenseError.get() != null
                        viewmodelRequestLicenseErrorJavaLangObjectNull = (viewmodelRequestLicenseErrorGet) != (null);
                    if((dirtyFlags & 0x512L) != 0) {
                        if(viewmodelRequestLicenseErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x1000000000L;
                        }
                        else {
                                dirtyFlags |= 0x800000000L;
                        }
                    }


                        // read viewmodel.request.licenseError.get() != null ? true : false
                        viewmodelRequestLicenseErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestLicenseErrorJavaLangObjectNull) ? (true) : (false));
                }
                if ((dirtyFlags & 0x514L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.commercialError
                            viewmodelRequestCommercialError = viewmodelRequest.commercialError;
                        }
                        updateRegistration(2, viewmodelRequestCommercialError);


                        if (viewmodelRequestCommercialError != null) {
                            // read viewmodel.request.commercialError.get()
                            viewmodelRequestCommercialErrorGet = viewmodelRequestCommercialError.get();
                        }


                        // read viewmodel.request.commercialError.get() != null
                        viewmodelRequestCommercialErrorJavaLangObjectNull = (viewmodelRequestCommercialErrorGet) != (null);
                    if((dirtyFlags & 0x514L) != 0) {
                        if(viewmodelRequestCommercialErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x4000000L;
                        }
                        else {
                                dirtyFlags |= 0x2000000L;
                        }
                    }


                        // read viewmodel.request.commercialError.get() != null ? true : false
                        viewmodelRequestCommercialErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestCommercialErrorJavaLangObjectNull) ? (true) : (false));
                }
                if ((dirtyFlags & 0x518L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.taxError
                            viewmodelRequestTaxError = viewmodelRequest.taxError;
                        }
                        updateRegistration(3, viewmodelRequestTaxError);


                        if (viewmodelRequestTaxError != null) {
                            // read viewmodel.request.taxError.get()
                            viewmodelRequestTaxErrorGet = viewmodelRequestTaxError.get();
                        }


                        // read viewmodel.request.taxError.get() != null
                        viewmodelRequestTaxErrorJavaLangObjectNull = (viewmodelRequestTaxErrorGet) != (null);
                    if((dirtyFlags & 0x518L) != 0) {
                        if(viewmodelRequestTaxErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x100000L;
                        }
                        else {
                                dirtyFlags |= 0x80000L;
                        }
                    }


                        // read viewmodel.request.taxError.get() != null ? true : false
                        viewmodelRequestTaxErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestTaxErrorJavaLangObjectNull) ? (true) : (false));
                }
                if ((dirtyFlags & 0x530L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.passportError
                            viewmodelRequestPassportError = viewmodelRequest.passportError;
                        }
                        updateRegistration(5, viewmodelRequestPassportError);


                        if (viewmodelRequestPassportError != null) {
                            // read viewmodel.request.passportError.get()
                            viewmodelRequestPassportErrorGet = viewmodelRequestPassportError.get();
                        }


                        // read viewmodel.request.passportError.get() != null
                        viewmodelRequestPassportErrorJavaLangObjectNull = (viewmodelRequestPassportErrorGet) != (null);
                    if((dirtyFlags & 0x530L) != 0) {
                        if(viewmodelRequestPassportErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x400000000L;
                        }
                        else {
                                dirtyFlags |= 0x200000000L;
                        }
                    }


                        // read viewmodel.request.passportError.get() != null ? true : false
                        viewmodelRequestPassportErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestPassportErrorJavaLangObjectNull) ? (true) : (false));
                }
                if ((dirtyFlags & 0x550L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.licenseBackError
                            viewmodelRequestLicenseBackError = viewmodelRequest.licenseBackError;
                        }
                        updateRegistration(6, viewmodelRequestLicenseBackError);


                        if (viewmodelRequestLicenseBackError != null) {
                            // read viewmodel.request.licenseBackError.get()
                            viewmodelRequestLicenseBackErrorGet = viewmodelRequestLicenseBackError.get();
                        }


                        // read viewmodel.request.licenseBackError.get() != null
                        viewmodelRequestLicenseBackErrorJavaLangObjectNull = (viewmodelRequestLicenseBackErrorGet) != (null);
                    if((dirtyFlags & 0x550L) != 0) {
                        if(viewmodelRequestLicenseBackErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x1000L;
                        }
                        else {
                                dirtyFlags |= 0x800L;
                        }
                    }


                        // read viewmodel.request.licenseBackError.get() != null ? true : false
                        viewmodelRequestLicenseBackErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestLicenseBackErrorJavaLangObjectNull) ? (true) : (false));
                }
                if ((dirtyFlags & 0x590L) != 0) {

                        if (viewmodelRequest != null) {
                            // read viewmodel.request.identityError
                            viewmodelRequestIdentityError = viewmodelRequest.identityError;
                        }
                        updateRegistration(7, viewmodelRequestIdentityError);


                        if (viewmodelRequestIdentityError != null) {
                            // read viewmodel.request.identityError.get()
                            viewmodelRequestIdentityErrorGet = viewmodelRequestIdentityError.get();
                        }


                        // read viewmodel.request.identityError.get() != null
                        viewmodelRequestIdentityErrorJavaLangObjectNull = (viewmodelRequestIdentityErrorGet) != (null);
                    if((dirtyFlags & 0x590L) != 0) {
                        if(viewmodelRequestIdentityErrorJavaLangObjectNull) {
                                dirtyFlags |= 0x10000000L;
                        }
                        else {
                                dirtyFlags |= 0x8000000L;
                        }
                    }


                        // read viewmodel.request.identityError.get() != null ? true : false
                        viewmodelRequestIdentityErrorJavaLangObjectNullBooleanTrueBooleanFalse = ((viewmodelRequestIdentityErrorJavaLangObjectNull) ? (true) : (false));
                }
            }
        }
        if ((dirtyFlags & 0x400L) != 0) {

                // read UserHelper.getInstance(context).accountType
                userHelperGetInstanceContextAccountType = grand.app.aber_provider.utils.session.UserHelper.getInstance(getRoot().getContext()).getAccountType();


                if (userHelperGetInstanceContextAccountType != null) {
                    // read UserHelper.getInstance(context).accountType.equals("0")
                    userHelperGetInstanceContextAccountTypeEqualsJavaLangString0 = userHelperGetInstanceContextAccountType.equals("0");
                    // read UserHelper.getInstance(context).accountType.equals("1")
                    userHelperGetInstanceContextAccountTypeEqualsJavaLangString1 = userHelperGetInstanceContextAccountType.equals("1");
                }
            if((dirtyFlags & 0x400L) != 0) {
                if(userHelperGetInstanceContextAccountTypeEqualsJavaLangString0) {
                        dirtyFlags |= 0x10000L;
                }
                else {
                        dirtyFlags |= 0x8000L;
                }
            }
            if((dirtyFlags & 0x400L) != 0) {
                if(userHelperGetInstanceContextAccountTypeEqualsJavaLangString1) {
                        dirtyFlags |= 0x40000000L;
                }
                else {
                        dirtyFlags |= 0x20000000L;
                }
            }


                // read UserHelper.getInstance(context).accountType.equals("0") ? View.GONE : View.VISIBLE
                userHelperGetInstanceContextAccountTypeEqualsJavaLangString0ViewGONEViewVISIBLE = ((userHelperGetInstanceContextAccountTypeEqualsJavaLangString0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read UserHelper.getInstance(context).accountType.equals("1") ? View.GONE : View.VISIBLE
                userHelperGetInstanceContextAccountTypeEqualsJavaLangString1ViewGONEViewVISIBLE = ((userHelperGetInstanceContextAccountTypeEqualsJavaLangString1) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished

        if ((dirtyFlags & 0x4000000000L) != 0) {

                if (viewmodelMessage != null) {
                    // read viewmodel.message.equals(Constants.SHOW_PROGRESS)
                    viewmodelMessageEqualsConstantsSHOWPROGRESS = viewmodelMessage.equals(grand.app.aber_provider.utils.Constants.SHOW_PROGRESS);
                }
        }
        if ((dirtyFlags & 0x2000L) != 0) {

                if (viewmodelMessage != null) {
                    // read viewmodel.message.equals(Constants.HIDE_PROGRESS)
                    viewmodelMessageEqualsConstantsHIDEPROGRESS = viewmodelMessage.equals(grand.app.aber_provider.utils.Constants.HIDE_PROGRESS);
                }
        }

        if ((dirtyFlags & 0x610L) != 0) {

                // read TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS)
                textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESS = ((textUtilsIsEmptyViewmodelMessage) ? (true) : (viewmodelMessageEqualsConstantsHIDEPROGRESS));
                // read !TextUtils.isEmpty(viewmodel.message) ? viewmodel.message.equals(Constants.SHOW_PROGRESS) : false
                textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((TextUtilsIsEmptyViewmodelMessage1) ? (viewmodelMessageEqualsConstantsSHOWPROGRESS) : (false));
            if((dirtyFlags & 0x610L) != 0) {
                if(textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESS) {
                        dirtyFlags |= 0x40000L;
                        dirtyFlags |= 0x100000000L;
                }
                else {
                        dirtyFlags |= 0x20000L;
                        dirtyFlags |= 0x80000000L;
                }
            }
            if((dirtyFlags & 0x610L) != 0) {
                if(textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x400000L;
                }
                else {
                        dirtyFlags |= 0x200000L;
                }
            }


                // read TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS) ? true : false
                textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse = ((textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESS) ? (true) : (false));
                // read TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
                textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESSAppCompatButtonNextAndroidDrawableCornerViewGradientAppCompatButtonNextAndroidDrawableCornerViewPrimaryMedium = ((textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESS) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(appCompatButtonNext.getContext(), R.drawable.corner_view_gradient)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(appCompatButtonNext.getContext(), R.drawable.corner_view_primary_medium)));
                // read !TextUtils.isEmpty(viewmodel.message) ? viewmodel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x610L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.appCompatButtonNext, textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESSAppCompatButtonNextAndroidDrawableCornerViewGradientAppCompatButtonNextAndroidDrawableCornerViewPrimaryMedium);
            this.appCompatButtonNext.setEnabled(textUtilsIsEmptyViewmodelMessageBooleanTrueViewmodelMessageEqualsConstantsHIDEPROGRESSBooleanTrueBooleanFalse);
            this.progress.setVisibility(textUtilsIsEmptyViewmodelMessageViewmodelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x400L) != 0) {
            // api target 1

            this.appCompatButtonNext.setOnClickListener(mCallback90);
            this.auto.setOnClickListener(mCallback83);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.auto, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, autoandroidTextAttrChanged);
            this.inputCompanyName.setVisibility(userHelperGetInstanceContextAccountTypeEqualsJavaLangString0ViewGONEViewVISIBLE);
            this.inputDriveBack.setVisibility(userHelperGetInstanceContextAccountTypeEqualsJavaLangString1ViewGONEViewVISIBLE);
            this.inputDriveFront.setVisibility(userHelperGetInstanceContextAccountTypeEqualsJavaLangString1ViewGONEViewVISIBLE);
            this.inputName.setVisibility(userHelperGetInstanceContextAccountTypeEqualsJavaLangString0ViewGONEViewVISIBLE);
            this.mboundView10.setOnClickListener(mCallback87);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView10, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView10androidTextAttrChanged);
            this.mboundView12.setOnClickListener(mCallback88);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView12, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView12androidTextAttrChanged);
            this.mboundView14.setOnClickListener(mCallback89);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView14, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView14androidTextAttrChanged);
            this.mboundView4.setOnClickListener(mCallback84);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            this.mboundView6.setOnClickListener(mCallback85);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView6, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView6androidTextAttrChanged);
            this.mboundView8.setOnClickListener(mCallback86);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView8, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView8androidTextAttrChanged);
        }
        if ((dirtyFlags & 0x510L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.auto, viewmodelRequestCommercialRegister);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView10, viewmodelRequestPassportImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView12, viewmodelRequestLicenseImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView14, viewmodelRequestLicenseBackImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewmodelRequestTaxImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, viewmodelRequestIdentityImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, viewmodelRequestIdentityBackImage);
        }
        if ((dirtyFlags & 0x518L) != 0) {
            // api target 1

            this.inputCompanyName.setErrorEnabled(viewmodelRequestTaxErrorJavaLangObjectNullBooleanTrueBooleanFalse);
            this.inputCompanyName.setError(viewmodelRequestTaxErrorGet);
        }
        if ((dirtyFlags & 0x550L) != 0) {
            // api target 1

            this.inputDriveBack.setErrorEnabled(viewmodelRequestLicenseBackErrorJavaLangObjectNullBooleanTrueBooleanFalse);
            this.inputDriveBack.setError(viewmodelRequestLicenseBackErrorGet);
        }
        if ((dirtyFlags & 0x512L) != 0) {
            // api target 1

            this.inputDriveFront.setErrorEnabled(viewmodelRequestLicenseErrorJavaLangObjectNullBooleanTrueBooleanFalse);
            this.inputDriveFront.setError(viewmodelRequestLicenseErrorGet);
        }
        if ((dirtyFlags & 0x511L) != 0) {
            // api target 1

            this.inputIdentityBack.setError(viewmodelRequestIdentityBackErrorGet);
            this.inputIdentityBack.setErrorEnabled(viewmodelRequestIdentityBackErrorJavaLangObjectNullBooleanTrueBooleanFalse);
        }
        if ((dirtyFlags & 0x590L) != 0) {
            // api target 1

            this.inputIdentityFront.setErrorEnabled(viewmodelRequestIdentityErrorJavaLangObjectNullBooleanTrueBooleanFalse);
            this.inputIdentityFront.setError(viewmodelRequestIdentityErrorGet);
        }
        if ((dirtyFlags & 0x514L) != 0) {
            // api target 1

            this.inputName.setErrorEnabled(viewmodelRequestCommercialErrorJavaLangObjectNullBooleanTrueBooleanFalse);
            this.inputName.setError(viewmodelRequestCommercialErrorGet);
        }
        if ((dirtyFlags & 0x530L) != 0) {
            // api target 1

            this.inputPassport.setErrorEnabled(viewmodelRequestPassportErrorJavaLangObjectNullBooleanTrueBooleanFalse);
            this.inputPassport.setError(viewmodelRequestPassportErrorGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.COMMERCIAL_IMAGE);
                }
                break;
            }
            case 7: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.LICENSE_BACK_IMAGE);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.PASSPORT_PHOTO);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.NATIONAL_BACK_ID_PHOTO);
                }
                break;
            }
            case 8: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {


                    viewmodel.registerDoc();
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.LICENSE_IMAGE);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.TAX_CARD_PHOTO);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewmodel != null
                boolean viewmodelJavaLangObjectNull = false;
                // viewmodel
                grand.app.aber_provider.pages.auth.register.RegisterViewModel viewmodel = mViewmodel;



                viewmodelJavaLangObjectNull = (viewmodel) != (null);
                if (viewmodelJavaLangObjectNull) {




                    viewmodel.imageSubmit(grand.app.aber_provider.utils.Constants.NATIONAL_ID_PHOTO);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel.request.identityBackError
        flag 1 (0x2L): viewmodel.request.licenseError
        flag 2 (0x3L): viewmodel.request.commercialError
        flag 3 (0x4L): viewmodel.request.taxError
        flag 4 (0x5L): viewmodel
        flag 5 (0x6L): viewmodel.request.passportError
        flag 6 (0x7L): viewmodel.request.licenseBackError
        flag 7 (0x8L): viewmodel.request.identityError
        flag 8 (0x9L): viewmodel.request
        flag 9 (0xaL): viewmodel.message
        flag 10 (0xbL): null
        flag 11 (0xcL): viewmodel.request.licenseBackError.get() != null ? true : false
        flag 12 (0xdL): viewmodel.request.licenseBackError.get() != null ? true : false
        flag 13 (0xeL): TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS)
        flag 14 (0xfL): TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS)
        flag 15 (0x10L): UserHelper.getInstance(context).accountType.equals("0") ? View.GONE : View.VISIBLE
        flag 16 (0x11L): UserHelper.getInstance(context).accountType.equals("0") ? View.GONE : View.VISIBLE
        flag 17 (0x12L): TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 18 (0x13L): TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS) ? true : false
        flag 19 (0x14L): viewmodel.request.taxError.get() != null ? true : false
        flag 20 (0x15L): viewmodel.request.taxError.get() != null ? true : false
        flag 21 (0x16L): !TextUtils.isEmpty(viewmodel.message) ? viewmodel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 22 (0x17L): !TextUtils.isEmpty(viewmodel.message) ? viewmodel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 23 (0x18L): viewmodel.request.identityBackError.get() != null ? true : false
        flag 24 (0x19L): viewmodel.request.identityBackError.get() != null ? true : false
        flag 25 (0x1aL): viewmodel.request.commercialError.get() != null ? true : false
        flag 26 (0x1bL): viewmodel.request.commercialError.get() != null ? true : false
        flag 27 (0x1cL): viewmodel.request.identityError.get() != null ? true : false
        flag 28 (0x1dL): viewmodel.request.identityError.get() != null ? true : false
        flag 29 (0x1eL): UserHelper.getInstance(context).accountType.equals("1") ? View.GONE : View.VISIBLE
        flag 30 (0x1fL): UserHelper.getInstance(context).accountType.equals("1") ? View.GONE : View.VISIBLE
        flag 31 (0x20L): TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 32 (0x21L): TextUtils.isEmpty(viewmodel.message) ? true : viewmodel.message.equals(Constants.HIDE_PROGRESS) ? @android:drawable/corner_view_gradient : @android:drawable/corner_view_primary_medium
        flag 33 (0x22L): viewmodel.request.passportError.get() != null ? true : false
        flag 34 (0x23L): viewmodel.request.passportError.get() != null ? true : false
        flag 35 (0x24L): viewmodel.request.licenseError.get() != null ? true : false
        flag 36 (0x25L): viewmodel.request.licenseError.get() != null ? true : false
        flag 37 (0x26L): !TextUtils.isEmpty(viewmodel.message) ? viewmodel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 38 (0x27L): !TextUtils.isEmpty(viewmodel.message) ? viewmodel.message.equals(Constants.SHOW_PROGRESS) : false
    flag mapping end*/
    //end
}