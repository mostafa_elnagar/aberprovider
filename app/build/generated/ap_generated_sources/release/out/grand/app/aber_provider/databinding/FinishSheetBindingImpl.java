package grand.app.aber_provider.databinding;
import grand.app.aber_provider.R;
import grand.app.aber_provider.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FinishSheetBindingImpl extends FinishSheetBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btn_accept, 5);
        sViewsWithIds.put(R.id.btn_reject, 6);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener tvReasonsandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.orderStatusRequest.reason
            //         is viewmodel.orderStatusRequest.setReason((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(tvReasons);
            // localize variables for thread safety
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.orderStatusRequest.reason
            java.lang.String viewmodelOrderStatusRequestReason = null;
            // viewmodel.orderStatusRequest != null
            boolean viewmodelOrderStatusRequestJavaLangObjectNull = false;
            // viewmodel.orderStatusRequest
            grand.app.aber_provider.pages.home.models.OrderStatusRequest viewmodelOrderStatusRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewmodel = mViewmodel;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelOrderStatusRequest = viewmodel.getOrderStatusRequest();

                viewmodelOrderStatusRequestJavaLangObjectNull = (viewmodelOrderStatusRequest) != (null);
                if (viewmodelOrderStatusRequestJavaLangObjectNull) {




                    viewmodelOrderStatusRequest.setReason(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener tvWarningBodyandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewmodel.orderStatusRequest.code
            //         is viewmodel.orderStatusRequest.setCode((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(tvWarningBody);
            // localize variables for thread safety
            // viewmodel != null
            boolean viewmodelJavaLangObjectNull = false;
            // viewmodel.orderStatusRequest.code
            java.lang.String viewmodelOrderStatusRequestCode = null;
            // viewmodel.orderStatusRequest != null
            boolean viewmodelOrderStatusRequestJavaLangObjectNull = false;
            // viewmodel.orderStatusRequest
            grand.app.aber_provider.pages.home.models.OrderStatusRequest viewmodelOrderStatusRequest = null;
            // viewmodel
            grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewmodel = mViewmodel;



            viewmodelJavaLangObjectNull = (viewmodel) != (null);
            if (viewmodelJavaLangObjectNull) {


                viewmodelOrderStatusRequest = viewmodel.getOrderStatusRequest();

                viewmodelOrderStatusRequestJavaLangObjectNull = (viewmodelOrderStatusRequest) != (null);
                if (viewmodelOrderStatusRequestJavaLangObjectNull) {




                    viewmodelOrderStatusRequest.setCode(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FinishSheetBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private FinishSheetBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (com.google.android.material.button.MaterialButton) bindings[5]
            , (com.google.android.material.button.MaterialButton) bindings[6]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[2]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (com.chaos.view.PinView) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvHeaderWarning.setTag(null);
        this.tvLoginWarning.setTag(null);
        this.tvReasons.setTag(null);
        this.tvWarningBody.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewmodel == variableId) {
            setViewmodel((grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewmodel(@Nullable grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel Viewmodel) {
        updateRegistration(1, Viewmodel);
        this.mViewmodel = Viewmodel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewmodel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewmodelFinishStatus((androidx.databinding.ObservableField<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeViewmodel((grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewmodelFinishStatus(androidx.databinding.ObservableField<java.lang.Integer> ViewmodelFinishStatus, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewmodel(grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel Viewmodel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.orderStatusRequest) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewmodelOrderStatusRequestCode = null;
        boolean viewmodelFinishStatusConstantsDONE = false;
        java.lang.String viewmodelFinishStatusConstantsNOTDONETvLoginWarningAndroidStringFinishNoCodeTvLoginWarningAndroidStringFinishWithCode = null;
        androidx.databinding.ObservableField<java.lang.Integer> viewmodelFinishStatus = null;
        int viewmodelFinishStatusConstantsNOTDONEViewVISIBLEViewGONE = 0;
        int androidxDatabindingViewDataBindingSafeUnboxViewmodelFinishStatusGet = 0;
        java.lang.Integer viewmodelFinishStatusGet = null;
        int viewmodelFinishStatusConstantsDONEViewVISIBLEViewGONE = 0;
        java.lang.String viewmodelOrderStatusRequestReason = null;
        grand.app.aber_provider.pages.home.models.OrderStatusRequest viewmodelOrderStatusRequest = null;
        boolean viewmodelFinishStatusConstantsNOTDONE = false;
        grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewmodel = mViewmodel;

        if ((dirtyFlags & 0xfL) != 0) {


            if ((dirtyFlags & 0xbL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.finishStatus
                        viewmodelFinishStatus = viewmodel.finishStatus;
                    }
                    updateRegistration(0, viewmodelFinishStatus);


                    if (viewmodelFinishStatus != null) {
                        // read viewmodel.finishStatus.get()
                        viewmodelFinishStatusGet = viewmodelFinishStatus.get();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get())
                    androidxDatabindingViewDataBindingSafeUnboxViewmodelFinishStatusGet = androidx.databinding.ViewDataBinding.safeUnbox(viewmodelFinishStatusGet);


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.DONE
                    viewmodelFinishStatusConstantsDONE = (androidxDatabindingViewDataBindingSafeUnboxViewmodelFinishStatusGet) == (grand.app.aber_provider.utils.Constants.DONE);
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE
                    viewmodelFinishStatusConstantsNOTDONE = (androidxDatabindingViewDataBindingSafeUnboxViewmodelFinishStatusGet) == (grand.app.aber_provider.utils.Constants.NOT_DONE);
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelFinishStatusConstantsDONE) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }
                if((dirtyFlags & 0xbL) != 0) {
                    if(viewmodelFinishStatusConstantsNOTDONE) {
                            dirtyFlags |= 0x20L;
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x10L;
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.DONE ? View.VISIBLE : View.GONE
                    viewmodelFinishStatusConstantsDONEViewVISIBLEViewGONE = ((viewmodelFinishStatusConstantsDONE) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE ? @android:string/finish_no_code : @android:string/finish_with_code
                    viewmodelFinishStatusConstantsNOTDONETvLoginWarningAndroidStringFinishNoCodeTvLoginWarningAndroidStringFinishWithCode = ((viewmodelFinishStatusConstantsNOTDONE) ? (tvLoginWarning.getResources().getString(R.string.finish_no_code)) : (tvLoginWarning.getResources().getString(R.string.finish_with_code)));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE ? View.VISIBLE : View.GONE
                    viewmodelFinishStatusConstantsNOTDONEViewVISIBLEViewGONE = ((viewmodelFinishStatusConstantsNOTDONE) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xeL) != 0) {

                    if (viewmodel != null) {
                        // read viewmodel.orderStatusRequest
                        viewmodelOrderStatusRequest = viewmodel.getOrderStatusRequest();
                    }


                    if (viewmodelOrderStatusRequest != null) {
                        // read viewmodel.orderStatusRequest.code
                        viewmodelOrderStatusRequestCode = viewmodelOrderStatusRequest.getCode();
                        // read viewmodel.orderStatusRequest.reason
                        viewmodelOrderStatusRequestReason = viewmodelOrderStatusRequest.getReason();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0xbL) != 0) {
            // api target 1

            this.tvHeaderWarning.setVisibility(viewmodelFinishStatusConstantsDONEViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvLoginWarning, viewmodelFinishStatusConstantsNOTDONETvLoginWarningAndroidStringFinishNoCodeTvLoginWarningAndroidStringFinishWithCode);
            this.tvReasons.setVisibility(viewmodelFinishStatusConstantsNOTDONEViewVISIBLEViewGONE);
            this.tvWarningBody.setVisibility(viewmodelFinishStatusConstantsDONEViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xeL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvReasons, viewmodelOrderStatusRequestReason);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvWarningBody, viewmodelOrderStatusRequestCode);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.tvReasons, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, tvReasonsandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.tvWarningBody, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, tvWarningBodyandroidTextAttrChanged);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewmodel.finishStatus
        flag 1 (0x2L): viewmodel
        flag 2 (0x3L): viewmodel.orderStatusRequest
        flag 3 (0x4L): null
        flag 4 (0x5L): androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE ? @android:string/finish_no_code : @android:string/finish_with_code
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE ? @android:string/finish_no_code : @android:string/finish_with_code
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.NOT_DONE ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.DONE ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewmodel.finishStatus.get()) == Constants.DONE ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}