// Generated by Dagger (https://dagger.dev).
package grand.app.aber_provider.pages.myOrders.viewModels;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_provider.repository.ServicesRepository;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MyOrdersViewModels_MembersInjector implements MembersInjector<MyOrdersViewModels> {
  private final Provider<ServicesRepository> postRepositoryProvider;

  public MyOrdersViewModels_MembersInjector(Provider<ServicesRepository> postRepositoryProvider) {
    this.postRepositoryProvider = postRepositoryProvider;
  }

  public static MembersInjector<MyOrdersViewModels> create(
      Provider<ServicesRepository> postRepositoryProvider) {
    return new MyOrdersViewModels_MembersInjector(postRepositoryProvider);
  }

  @Override
  public void injectMembers(MyOrdersViewModels instance) {
    injectPostRepository(instance, postRepositoryProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_provider.pages.myOrders.viewModels.MyOrdersViewModels.postRepository")
  public static void injectPostRepository(MyOrdersViewModels instance,
      ServicesRepository postRepository) {
    instance.postRepository = postRepository;
  }
}
