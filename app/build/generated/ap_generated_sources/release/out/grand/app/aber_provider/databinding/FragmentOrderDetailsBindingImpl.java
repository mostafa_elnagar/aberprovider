package grand.app.aber_provider.databinding;
import grand.app.aber_provider.R;
import grand.app.aber_provider.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOrderDetailsBindingImpl extends FragmentOrderDetailsBinding implements grand.app.aber_provider.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.searchContainer, 42);
        sViewsWithIds.put(R.id.textView2, 43);
        sViewsWithIds.put(R.id.card_personal_info, 44);
        sViewsWithIds.put(R.id.header, 45);
        sViewsWithIds.put(R.id.card_service, 46);
        sViewsWithIds.put(R.id.tv_service_type, 47);
        sViewsWithIds.put(R.id.card_time, 48);
        sViewsWithIds.put(R.id.item_time_warning, 49);
        sViewsWithIds.put(R.id.tv_delivery_time, 50);
        sViewsWithIds.put(R.id.br, 51);
        sViewsWithIds.put(R.id.required_service_type, 52);
        sViewsWithIds.put(R.id.car_desc, 53);
        sViewsWithIds.put(R.id.car_image_desc, 54);
        sViewsWithIds.put(R.id.car_type, 55);
        sViewsWithIds.put(R.id.car_cat, 56);
        sViewsWithIds.put(R.id.ca_year, 57);
        sViewsWithIds.put(R.id.other_service, 58);
        sViewsWithIds.put(R.id.extra_service, 59);
        sViewsWithIds.put(R.id.card_invoice, 60);
        sViewsWithIds.put(R.id.tv_services_cost, 61);
        sViewsWithIds.put(R.id.v_services_price, 62);
        sViewsWithIds.put(R.id.tv_services_extra_cost, 63);
        sViewsWithIds.put(R.id.v_services__extra_price, 64);
        sViewsWithIds.put(R.id.tv_delivery_cost, 65);
        sViewsWithIds.put(R.id.v_delivery_price, 66);
        sViewsWithIds.put(R.id.tv_eme_cost, 67);
        sViewsWithIds.put(R.id.v_eme_price, 68);
        sViewsWithIds.put(R.id.tv_total, 69);
        sViewsWithIds.put(R.id.card_desc, 70);
        sViewsWithIds.put(R.id.tv_service_desc, 71);
        sViewsWithIds.put(R.id.br2, 72);
        sViewsWithIds.put(R.id.flow3, 73);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final grand.app.aber_provider.customViews.views.CustomTextViewRegular mboundView10;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView12;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView18;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView20;
    @NonNull
    private final com.makeramen.roundedimageview.RoundedImageView mboundView28;
    @NonNull
    private final androidx.cardview.widget.CardView mboundView36;
    @NonNull
    private final grand.app.aber_provider.customViews.views.CustomTextViewRegular mboundView41;
    @NonNull
    private final androidx.constraintlayout.helper.widget.Flow mboundView6;
    @NonNull
    private final grand.app.aber_provider.customViews.views.CustomTextViewRegular mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback94;
    @Nullable
    private final android.view.View.OnClickListener mCallback95;
    @Nullable
    private final android.view.View.OnClickListener mCallback92;
    @Nullable
    private final android.view.View.OnClickListener mCallback93;
    @Nullable
    private final android.view.View.OnClickListener mCallback91;
    @Nullable
    private final android.view.View.OnClickListener mCallback96;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOrderDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 74, sIncludes, sViewsWithIds));
    }
    private FragmentOrderDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.constraintlayout.widget.Barrier) bindings[51]
            , (android.view.View) bindings[72]
            , (com.google.android.material.button.MaterialButton) bindings[31]
            , (com.google.android.material.button.MaterialButton) bindings[32]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[57]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[16]
            , (com.google.android.material.button.MaterialButton) bindings[5]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[56]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[15]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[53]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[54]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[55]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[14]
            , (androidx.cardview.widget.CardView) bindings[26]
            , (androidx.cardview.widget.CardView) bindings[13]
            , (androidx.cardview.widget.CardView) bindings[70]
            , (androidx.cardview.widget.CardView) bindings[19]
            , (androidx.cardview.widget.CardView) bindings[60]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (androidx.cardview.widget.CardView) bindings[17]
            , (android.widget.RelativeLayout) bindings[44]
            , (androidx.cardview.widget.CardView) bindings[46]
            , (androidx.cardview.widget.CardView) bindings[48]
            , (com.google.android.material.button.MaterialButton) bindings[4]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[40]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[59]
            , (androidx.constraintlayout.helper.widget.Flow) bindings[73]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[45]
            , (com.google.android.material.imageview.ShapeableImageView) bindings[37]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[8]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[49]
            , (com.makeramen.roundedimageview.RoundedImageView) bindings[30]
            , (androidx.appcompat.widget.AppCompatRatingBar) bindings[39]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[58]
            , (com.google.android.material.progressindicator.CircularProgressIndicator) bindings[34]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[52]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[42]
            , (com.google.android.material.button.MaterialButton) bindings[33]
            , (android.widget.TextView) bindings[43]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[27]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[65]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[23]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[50]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[67]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[24]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[2]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[3]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[35]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[38]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[71]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[29]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[47]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[61]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[63]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[22]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[69]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[25]
            , (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[21]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[7]
            , (android.view.View) bindings[66]
            , (android.view.View) bindings[68]
            , (android.view.View) bindings[64]
            , (android.view.View) bindings[62]
            );
        this.btnAccept.setTag(null);
        this.btnReject.setTag(null);
        this.caYearValue.setTag(null);
        this.callBtn.setTag(null);
        this.carCatValue.setTag(null);
        this.carTypeValue.setTag(null);
        this.cardBattery.setTag(null);
        this.cardCarDesc.setTag(null);
        this.cardExtraService.setTag(null);
        this.cardMainService.setTag(null);
        this.cardOtherService.setTag(null);
        this.chatBtn.setTag(null);
        this.createAt.setTag(null);
        this.icProviderRate.setTag(null);
        this.itemImg.setTag(null);
        this.locationImage.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView10 = (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView12 = (androidx.recyclerview.widget.RecyclerView) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView18 = (androidx.recyclerview.widget.RecyclerView) bindings[18];
        this.mboundView18.setTag(null);
        this.mboundView20 = (androidx.recyclerview.widget.RecyclerView) bindings[20];
        this.mboundView20.setTag(null);
        this.mboundView28 = (com.makeramen.roundedimageview.RoundedImageView) bindings[28];
        this.mboundView28.setTag(null);
        this.mboundView36 = (androidx.cardview.widget.CardView) bindings[36];
        this.mboundView36.setTag(null);
        this.mboundView41 = (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[41];
        this.mboundView41.setTag(null);
        this.mboundView6 = (androidx.constraintlayout.helper.widget.Flow) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView9 = (grand.app.aber_provider.customViews.views.CustomTextViewRegular) bindings[9];
        this.mboundView9.setTag(null);
        this.myRate.setTag(null);
        this.progress.setTag(null);
        this.status.setTag(null);
        this.tvBattery.setTag(null);
        this.tvDeliveryPrice.setTag(null);
        this.tvEmePrice.setTag(null);
        this.tvHomeItem.setTag(null);
        this.tvHomeJob.setTag(null);
        this.tvMyReviewProvider.setTag(null);
        this.tvPrice.setTag(null);
        this.tvProviderName.setTag(null);
        this.tvServiceDescValue.setTag(null);
        this.tvServicesExtraPrice.setTag(null);
        this.tvTotalPrice.setTag(null);
        this.tvTotalServicesPrice.setTag(null);
        this.userProfileImg.setTag(null);
        setRootTag(root);
        // listeners
        mCallback94 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 4);
        mCallback95 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 5);
        mCallback92 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 2);
        mCallback93 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 3);
        mCallback91 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 1);
        mCallback96 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 6);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.orderDetailsMain) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.servicesRequiredAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.optionsDetailsAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.extraRequiredAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        else if (fieldId == BR.message) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        double viewModelOrderDetailsMainDeliveryFees = 0.0;
        boolean viewModelOrderDetailsMainStatusInt0 = false;
        java.lang.String viewModelOrderDetailsMainVehicleChild = null;
        boolean viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalse = false;
        boolean viewModelOrderDetailsMainSubServicesSizeInt0 = false;
        boolean textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = false;
        java.lang.String stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangString = null;
        java.lang.String stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangStringConcatViewModelCurrency = null;
        java.lang.String viewModelOrderDetailsMainUserName = null;
        boolean textUtilsIsEmptyViewModelOrderDetailsMainImage = false;
        grand.app.aber_provider.pages.orderDetails.models.OrderDetailsMain viewModelOrderDetailsMain = null;
        int viewModelOrderDetailsMainStatus = 0;
        java.util.List<grand.app.aber_provider.pages.orderDetails.models.SubServices> viewModelOrderDetailsMainExtraServices = null;
        boolean viewModelOrderDetailsMainIsEmergencyInt1 = false;
        java.lang.String viewModelOrderDetailsMainMainServiceIdInt9TvBatteryAndroidStringTierImageViewModelOrderDetailsMainMainServiceIdInt19TvBatteryAndroidStringLicenseCivilImageHintTvBatteryAndroidStringBatteryImage = null;
        int viewModelOrderDetailsMainChildServicesSize = 0;
        int textUtilsIsEmptyViewModelOrderDetailsMainImageViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewModelOrderDetailsMainEmergencyCost = null;
        int viewModelOrderDetailsMainCanceled = 0;
        java.lang.String viewModelOrderDetailsMainVehicleSubChild = null;
        grand.app.aber_provider.pages.orderDetails.adapters.OptionsDetailsAdapter viewModelOptionsDetailsAdapter = null;
        java.lang.String viewModelMessage = null;
        boolean viewModelOrderDetailsMainStatusInt4 = false;
        java.lang.String viewModelOrderDetailsMainReviewCreatedAt = null;
        boolean textUtilsIsEmptyViewModelMessage = false;
        int viewModelOrderDetailsMainMainServiceId = 0;
        java.lang.String stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangStringConcatViewModelCurrency = null;
        java.lang.String stringValueOfViewModelOrderDetailsMainSubtotal = null;
        boolean viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3 = false;
        int textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = 0;
        boolean viewModelOrderDetailsMainVehicleJavaLangObjectNull = false;
        grand.app.aber_provider.pages.orderDetails.adapters.ServicesRequiredAdapter viewModelExtraRequiredAdapter = null;
        java.lang.String viewModelOrderDetailsMainReviewRate = null;
        java.lang.String viewModelOrderDetailsMainUserImage = null;
        int viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalseViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangString = null;
        int viewModelOrderDetailsMainVehicleJavaLangObjectNullViewVISIBLEViewGONE = 0;
        grand.app.aber_provider.pages.orderDetails.models.MainServices viewModelOrderDetailsMainMainService = null;
        grand.app.aber_provider.pages.settings.models.rates.RatesItem viewModelOrderDetailsMainReview = null;
        java.lang.String viewModelOrderDetailsMainStatusInt0BtnAcceptAndroidStringAcceptBtnAcceptAndroidStringOrderFinished = null;
        int viewModelOrderDetailsMainSubServicesSizeInt0ViewVISIBLEViewGONE = 0;
        java.lang.String viewModelOrderDetailsMainMainServiceName = null;
        int viewModelOrderDetailsMainReviewJavaLangObjectNullViewGONEViewVISIBLE = 0;
        java.lang.String stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangStringConcatViewModelCurrency = null;
        double viewModelOrderDetailsMainExtraFees = 0.0;
        java.lang.String viewModelOrderDetailsMainAddress = null;
        grand.app.aber_provider.pages.auth.models.UserData viewModelOrderDetailsMainReviewUser = null;
        grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
        int viewModelOrderDetailsMainSubServicesSize = 0;
        boolean viewModelOrderDetailsMainMainServiceIdInt19 = false;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = false;
        java.lang.String viewModelOrderDetailsMainStatusText = null;
        java.lang.String viewModelOrderDetailsMainReviewComment = null;
        java.lang.String viewModelOrderDetailsMainMainServiceImage = null;
        java.lang.String stringValueOfViewModelOrderDetailsMainDeliveryFees = null;
        boolean ViewModelOrderDetailsMainStatusInt01 = false;
        boolean viewModelOrderDetailsMainStatusInt2 = false;
        java.lang.String stringValueOfViewModelOrderDetailsMainExtraFees = null;
        boolean TextUtilsIsEmptyViewModelOrderDetailsMainImage1 = false;
        java.lang.String viewModelOrderDetailsMainDescription = null;
        double viewModelOrderDetailsMainEmergencyCost = 0.0;
        java.lang.String viewModelOrderDetailsMainMainServiceIdInt19TvBatteryAndroidStringLicenseCivilImageHintTvBatteryAndroidStringBatteryImage = null;
        boolean viewModelOrderDetailsMainCanceledInt1 = false;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalseBooleanTrueBooleanFalse = false;
        boolean viewModelOrderDetailsMainChildServicesSizeInt0 = false;
        boolean viewModelOrderDetailsMainExtraServicesSizeInt0 = false;
        double viewModelOrderDetailsMainSubtotal = 0.0;
        java.lang.String viewModelOrderDetailsMainStatusInt0BtnRejectAndroidStringRejectBtnRejectAndroidStringNotDone = null;
        double viewModelOrderDetailsMainTotal = 0.0;
        int viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3ViewVISIBLEViewGONE = 0;
        boolean viewModelOrderDetailsMainStatusInt3 = false;
        int viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalseViewVISIBLEViewGONE = 0;
        java.lang.String stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangString = null;
        int viewModelOrderDetailsMainChildServicesSizeInt0ViewGONEViewVISIBLE = 0;
        boolean viewModelMessageEqualsConstantsSHOWPROGRESS = false;
        java.lang.String stringValueOfViewModelOrderDetailsMainTotal = null;
        grand.app.aber_provider.pages.orderDetails.adapters.ServicesRequiredAdapter viewModelServicesRequiredAdapter = null;
        int viewModelOrderDetailsMainExtraServicesSize = 0;
        java.lang.String viewModelOrderDetailsMainReviewUserName = null;
        java.lang.String viewModelCurrency = null;
        java.lang.String viewModelOrderDetailsMainImage = null;
        java.util.List<grand.app.aber_provider.pages.orderDetails.models.SubServices> viewModelOrderDetailsMainSubServices = null;
        boolean TextUtilsIsEmptyViewModelMessage1 = false;
        grand.app.aber_provider.pages.auth.models.UserData viewModelOrderDetailsMainUser = null;
        java.util.List<grand.app.aber_provider.pages.orderDetails.models.ChildServices> viewModelOrderDetailsMainChildServices = null;
        boolean viewModelOrderDetailsMainReviewJavaLangObjectNull = false;
        boolean viewModelOrderDetailsMainMainServiceIdInt9 = false;
        java.lang.String viewModelOrderDetailsMainStaticLocationImage = null;
        grand.app.aber_provider.pages.orderDetails.models.Vehicle viewModelOrderDetailsMainVehicle = null;
        boolean viewModelMessageEqualsConstantsHIDEPROGRESS = false;
        boolean viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalse = false;
        int viewModelOrderDetailsMainUserJavaLangObjectNullViewVISIBLEViewGONE = 0;
        int viewModelOrderDetailsMainIsEmergency = 0;
        boolean textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalse = false;
        java.lang.String viewModelOrderDetailsMainIsEmergencyInt1MboundView10AndroidStringEmergencyServicesViewModelOrderDetailsMainScheduledAt = null;
        java.lang.String viewModelOrderDetailsMainVehicleParent = null;
        boolean viewModelOrderDetailsMainUserJavaLangObjectNull = false;
        java.lang.String stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangStringConcatViewModelCurrency = null;
        java.lang.String stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangStringConcatViewModelCurrency = null;
        int viewModelOrderDetailsMainExtraServicesSizeInt0ViewGONEViewVISIBLE = 0;
        java.lang.String viewModelOrderDetailsMainScheduledAt = null;
        java.lang.String stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangString = null;
        java.lang.String stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangString = null;

        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x43L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.orderDetailsMain
                        viewModelOrderDetailsMain = viewModel.getOrderDetailsMain();
                        // read viewModel.currency
                        viewModelCurrency = viewModel.currency;
                    }


                    if (viewModelOrderDetailsMain != null) {
                        // read viewModel.orderDetailsMain.deliveryFees
                        viewModelOrderDetailsMainDeliveryFees = viewModelOrderDetailsMain.getDeliveryFees();
                        // read viewModel.orderDetailsMain.status
                        viewModelOrderDetailsMainStatus = viewModelOrderDetailsMain.getStatus();
                        // read viewModel.orderDetailsMain.extraServices
                        viewModelOrderDetailsMainExtraServices = viewModelOrderDetailsMain.getExtraServices();
                        // read viewModel.orderDetailsMain.mainService
                        viewModelOrderDetailsMainMainService = viewModelOrderDetailsMain.getMainService();
                        // read viewModel.orderDetailsMain.review
                        viewModelOrderDetailsMainReview = viewModelOrderDetailsMain.getReview();
                        // read viewModel.orderDetailsMain.extraFees
                        viewModelOrderDetailsMainExtraFees = viewModelOrderDetailsMain.getExtraFees();
                        // read viewModel.orderDetailsMain.address
                        viewModelOrderDetailsMainAddress = viewModelOrderDetailsMain.getAddress();
                        // read viewModel.orderDetailsMain.statusText
                        viewModelOrderDetailsMainStatusText = viewModelOrderDetailsMain.getStatusText();
                        // read viewModel.orderDetailsMain.description
                        viewModelOrderDetailsMainDescription = viewModelOrderDetailsMain.getDescription();
                        // read viewModel.orderDetailsMain.emergencyCost
                        viewModelOrderDetailsMainEmergencyCost = viewModelOrderDetailsMain.getEmergencyCost();
                        // read viewModel.orderDetailsMain.subtotal
                        viewModelOrderDetailsMainSubtotal = viewModelOrderDetailsMain.getSubtotal();
                        // read viewModel.orderDetailsMain.total
                        viewModelOrderDetailsMainTotal = viewModelOrderDetailsMain.getTotal();
                        // read viewModel.orderDetailsMain.image
                        viewModelOrderDetailsMainImage = viewModelOrderDetailsMain.getImage();
                        // read viewModel.orderDetailsMain.subServices
                        viewModelOrderDetailsMainSubServices = viewModelOrderDetailsMain.getSubServices();
                        // read viewModel.orderDetailsMain.user
                        viewModelOrderDetailsMainUser = viewModelOrderDetailsMain.getUser();
                        // read viewModel.orderDetailsMain.childServices
                        viewModelOrderDetailsMainChildServices = viewModelOrderDetailsMain.getChildServices();
                        // read viewModel.orderDetailsMain.staticLocationImage
                        viewModelOrderDetailsMainStaticLocationImage = viewModelOrderDetailsMain.getStaticLocationImage();
                        // read viewModel.orderDetailsMain.vehicle
                        viewModelOrderDetailsMainVehicle = viewModelOrderDetailsMain.getVehicle();
                        // read viewModel.orderDetailsMain.isEmergency
                        viewModelOrderDetailsMainIsEmergency = viewModelOrderDetailsMain.getIsEmergency();
                    }


                    // read String.valueOf(viewModel.orderDetailsMain.deliveryFees)
                    stringValueOfViewModelOrderDetailsMainDeliveryFees = java.lang.String.valueOf(viewModelOrderDetailsMainDeliveryFees);
                    // read viewModel.orderDetailsMain.status == 0
                    viewModelOrderDetailsMainStatusInt0 = (viewModelOrderDetailsMainStatus) == (0);
                    // read viewModel.orderDetailsMain.status != 0
                    ViewModelOrderDetailsMainStatusInt01 = (viewModelOrderDetailsMainStatus) != (0);
                    // read viewModel.orderDetailsMain.review == null
                    viewModelOrderDetailsMainReviewJavaLangObjectNull = (viewModelOrderDetailsMainReview) == (null);
                    // read String.valueOf(viewModel.orderDetailsMain.extraFees)
                    stringValueOfViewModelOrderDetailsMainExtraFees = java.lang.String.valueOf(viewModelOrderDetailsMainExtraFees);
                    // read String.valueOf(viewModel.orderDetailsMain.emergencyCost)
                    stringValueOfViewModelOrderDetailsMainEmergencyCost = java.lang.String.valueOf(viewModelOrderDetailsMainEmergencyCost);
                    // read String.valueOf(viewModel.orderDetailsMain.subtotal)
                    stringValueOfViewModelOrderDetailsMainSubtotal = java.lang.String.valueOf(viewModelOrderDetailsMainSubtotal);
                    // read String.valueOf(viewModel.orderDetailsMain.total)
                    stringValueOfViewModelOrderDetailsMainTotal = java.lang.String.valueOf(viewModelOrderDetailsMainTotal);
                    // read TextUtils.isEmpty(viewModel.orderDetailsMain.image)
                    TextUtilsIsEmptyViewModelOrderDetailsMainImage1 = android.text.TextUtils.isEmpty(viewModelOrderDetailsMainImage);
                    // read viewModel.orderDetailsMain.user != null
                    viewModelOrderDetailsMainUserJavaLangObjectNull = (viewModelOrderDetailsMainUser) != (null);
                    // read viewModel.orderDetailsMain.vehicle != null
                    viewModelOrderDetailsMainVehicleJavaLangObjectNull = (viewModelOrderDetailsMainVehicle) != (null);
                    // read viewModel.orderDetailsMain.isEmergency == 1
                    viewModelOrderDetailsMainIsEmergencyInt1 = (viewModelOrderDetailsMainIsEmergency) == (1);
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainStatusInt0) {
                            dirtyFlags |= 0x10000L;
                            dirtyFlags |= 0x1000000L;
                            dirtyFlags |= 0x1000000000L;
                    }
                    else {
                            dirtyFlags |= 0x8000L;
                            dirtyFlags |= 0x800000L;
                            dirtyFlags |= 0x800000000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(ViewModelOrderDetailsMainStatusInt01) {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x100000000000L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                            dirtyFlags |= 0x80000000000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainReviewJavaLangObjectNull) {
                            dirtyFlags |= 0x10000000L;
                    }
                    else {
                            dirtyFlags |= 0x8000000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainUserJavaLangObjectNull) {
                            dirtyFlags |= 0x400000000000L;
                    }
                    else {
                            dirtyFlags |= 0x200000000000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainVehicleJavaLangObjectNull) {
                            dirtyFlags |= 0x400000L;
                    }
                    else {
                            dirtyFlags |= 0x200000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainIsEmergencyInt1) {
                            dirtyFlags |= 0x4000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x2000000000000L;
                    }
                }
                    if (viewModelOrderDetailsMainExtraServices != null) {
                        // read viewModel.orderDetailsMain.extraServices.size()
                        viewModelOrderDetailsMainExtraServicesSize = viewModelOrderDetailsMainExtraServices.size();
                    }
                    if (viewModelOrderDetailsMainMainService != null) {
                        // read viewModel.orderDetailsMain.mainService.id
                        viewModelOrderDetailsMainMainServiceId = viewModelOrderDetailsMainMainService.getId();
                        // read viewModel.orderDetailsMain.mainService.name
                        viewModelOrderDetailsMainMainServiceName = viewModelOrderDetailsMainMainService.getName();
                        // read viewModel.orderDetailsMain.mainService.image
                        viewModelOrderDetailsMainMainServiceImage = viewModelOrderDetailsMainMainService.getImage();
                    }
                    if (viewModelOrderDetailsMainReview != null) {
                        // read viewModel.orderDetailsMain.review.createdAt
                        viewModelOrderDetailsMainReviewCreatedAt = viewModelOrderDetailsMainReview.getCreatedAt();
                        // read viewModel.orderDetailsMain.review.rate
                        viewModelOrderDetailsMainReviewRate = viewModelOrderDetailsMainReview.getRate();
                        // read viewModel.orderDetailsMain.review.user
                        viewModelOrderDetailsMainReviewUser = viewModelOrderDetailsMainReview.getUser();
                        // read viewModel.orderDetailsMain.review.comment
                        viewModelOrderDetailsMainReviewComment = viewModelOrderDetailsMainReview.getComment();
                    }
                    if (viewModelOrderDetailsMainSubServices != null) {
                        // read viewModel.orderDetailsMain.subServices.size()
                        viewModelOrderDetailsMainSubServicesSize = viewModelOrderDetailsMainSubServices.size();
                    }
                    if (viewModelOrderDetailsMainUser != null) {
                        // read viewModel.orderDetailsMain.user.name
                        viewModelOrderDetailsMainUserName = viewModelOrderDetailsMainUser.getName();
                        // read viewModel.orderDetailsMain.user.image
                        viewModelOrderDetailsMainUserImage = viewModelOrderDetailsMainUser.getImage();
                    }
                    if (viewModelOrderDetailsMainChildServices != null) {
                        // read viewModel.orderDetailsMain.childServices.size()
                        viewModelOrderDetailsMainChildServicesSize = viewModelOrderDetailsMainChildServices.size();
                    }
                    if (viewModelOrderDetailsMainVehicle != null) {
                        // read viewModel.orderDetailsMain.vehicle.child
                        viewModelOrderDetailsMainVehicleChild = viewModelOrderDetailsMainVehicle.getChild();
                        // read viewModel.orderDetailsMain.vehicle.sub_child
                        viewModelOrderDetailsMainVehicleSubChild = viewModelOrderDetailsMainVehicle.getSub_child();
                        // read viewModel.orderDetailsMain.vehicle.parent
                        viewModelOrderDetailsMainVehicleParent = viewModelOrderDetailsMainVehicle.getParent();
                    }


                    if (stringValueOfViewModelOrderDetailsMainDeliveryFees != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.deliveryFees).concat(" ")
                        stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangString = stringValueOfViewModelOrderDetailsMainDeliveryFees.concat(" ");
                    }
                    // read viewModel.orderDetailsMain.status == 0 ? @android:string/accept : @android:string/order_finished
                    viewModelOrderDetailsMainStatusInt0BtnAcceptAndroidStringAcceptBtnAcceptAndroidStringOrderFinished = ((viewModelOrderDetailsMainStatusInt0) ? (btnAccept.getResources().getString(R.string.accept)) : (btnAccept.getResources().getString(R.string.order_finished)));
                    // read viewModel.orderDetailsMain.status == 0 ? @android:string/reject : @android:string/not_done
                    viewModelOrderDetailsMainStatusInt0BtnRejectAndroidStringRejectBtnRejectAndroidStringNotDone = ((viewModelOrderDetailsMainStatusInt0) ? (btnReject.getResources().getString(R.string.reject)) : (btnReject.getResources().getString(R.string.not_done)));
                    // read viewModel.orderDetailsMain.review == null ? View.GONE : View.VISIBLE
                    viewModelOrderDetailsMainReviewJavaLangObjectNullViewGONEViewVISIBLE = ((viewModelOrderDetailsMainReviewJavaLangObjectNull) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read !TextUtils.isEmpty(viewModel.orderDetailsMain.image)
                    textUtilsIsEmptyViewModelOrderDetailsMainImage = !TextUtilsIsEmptyViewModelOrderDetailsMainImage1;
                    // read viewModel.orderDetailsMain.user != null ? View.VISIBLE : View.GONE
                    viewModelOrderDetailsMainUserJavaLangObjectNullViewVISIBLEViewGONE = ((viewModelOrderDetailsMainUserJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.orderDetailsMain.vehicle != null ? View.VISIBLE : View.GONE
                    viewModelOrderDetailsMainVehicleJavaLangObjectNullViewVISIBLEViewGONE = ((viewModelOrderDetailsMainVehicleJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.orderDetailsMain.extraServices.size() == 0
                    viewModelOrderDetailsMainExtraServicesSizeInt0 = (viewModelOrderDetailsMainExtraServicesSize) == (0);
                    // read viewModel.orderDetailsMain.mainService.id == 9
                    viewModelOrderDetailsMainMainServiceIdInt9 = (viewModelOrderDetailsMainMainServiceId) == (9);
                    // read viewModel.orderDetailsMain.subServices.size() > 0
                    viewModelOrderDetailsMainSubServicesSizeInt0 = (viewModelOrderDetailsMainSubServicesSize) > (0);
                    // read viewModel.orderDetailsMain.childServices.size() == 0
                    viewModelOrderDetailsMainChildServicesSizeInt0 = (viewModelOrderDetailsMainChildServicesSize) == (0);
                if((dirtyFlags & 0x43L) != 0) {
                    if(textUtilsIsEmptyViewModelOrderDetailsMainImage) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainExtraServicesSizeInt0) {
                            dirtyFlags |= 0x10000000000000L;
                    }
                    else {
                            dirtyFlags |= 0x8000000000000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainMainServiceIdInt9) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainSubServicesSizeInt0) {
                            dirtyFlags |= 0x4000000L;
                    }
                    else {
                            dirtyFlags |= 0x2000000L;
                    }
                }
                if((dirtyFlags & 0x43L) != 0) {
                    if(viewModelOrderDetailsMainChildServicesSizeInt0) {
                            dirtyFlags |= 0x40000000000L;
                    }
                    else {
                            dirtyFlags |= 0x20000000000L;
                    }
                }
                    if (stringValueOfViewModelOrderDetailsMainExtraFees != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.extraFees).concat(" ")
                        stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangString = stringValueOfViewModelOrderDetailsMainExtraFees.concat(" ");
                    }
                    if (stringValueOfViewModelOrderDetailsMainEmergencyCost != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.emergencyCost).concat(" ")
                        stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangString = stringValueOfViewModelOrderDetailsMainEmergencyCost.concat(" ");
                    }
                    if (stringValueOfViewModelOrderDetailsMainSubtotal != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.subtotal).concat(" ")
                        stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangString = stringValueOfViewModelOrderDetailsMainSubtotal.concat(" ");
                    }
                    if (stringValueOfViewModelOrderDetailsMainTotal != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.total).concat(" ")
                        stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangString = stringValueOfViewModelOrderDetailsMainTotal.concat(" ");
                    }
                    if (viewModelOrderDetailsMainReviewUser != null) {
                        // read viewModel.orderDetailsMain.review.user.name
                        viewModelOrderDetailsMainReviewUserName = viewModelOrderDetailsMainReviewUser.getName();
                    }


                    if (stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.deliveryFees).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangString.concat(viewModelCurrency);
                    }
                    // read !TextUtils.isEmpty(viewModel.orderDetailsMain.image) ? View.VISIBLE : View.GONE
                    textUtilsIsEmptyViewModelOrderDetailsMainImageViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelOrderDetailsMainImage) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.orderDetailsMain.extraServices.size() == 0 ? View.GONE : View.VISIBLE
                    viewModelOrderDetailsMainExtraServicesSizeInt0ViewGONEViewVISIBLE = ((viewModelOrderDetailsMainExtraServicesSizeInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.orderDetailsMain.subServices.size() > 0 ? View.VISIBLE : View.GONE
                    viewModelOrderDetailsMainSubServicesSizeInt0ViewVISIBLEViewGONE = ((viewModelOrderDetailsMainSubServicesSizeInt0) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.orderDetailsMain.childServices.size() == 0 ? View.GONE : View.VISIBLE
                    viewModelOrderDetailsMainChildServicesSizeInt0ViewGONEViewVISIBLE = ((viewModelOrderDetailsMainChildServicesSizeInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    if (stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.extraFees).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangString.concat(viewModelCurrency);
                    }
                    if (stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.emergencyCost).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangString.concat(viewModelCurrency);
                    }
                    if (stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.subtotal).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangString.concat(viewModelCurrency);
                    }
                    if (stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangString != null) {
                        // read String.valueOf(viewModel.orderDetailsMain.total).concat(" ").concat(viewModel.currency)
                        stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangStringConcatViewModelCurrency = stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangString.concat(viewModelCurrency);
                    }
            }
            if ((dirtyFlags & 0x49L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.optionsDetailsAdapter
                        viewModelOptionsDetailsAdapter = viewModel.getOptionsDetailsAdapter();
                    }
            }
            if ((dirtyFlags & 0x63L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.message
                        viewModelMessage = viewModel.getMessage();
                    }


                    // read TextUtils.isEmpty(viewModel.message)
                    textUtilsIsEmptyViewModelMessage = android.text.TextUtils.isEmpty(viewModelMessage);
                if((dirtyFlags & 0x63L) != 0) {
                    if(textUtilsIsEmptyViewModelMessage) {
                            dirtyFlags |= 0x40000000L;
                    }
                    else {
                            dirtyFlags |= 0x20000000L;
                    }
                }

                if ((dirtyFlags & 0x61L) != 0) {

                        // read !TextUtils.isEmpty(viewModel.message)
                        TextUtilsIsEmptyViewModelMessage1 = !textUtilsIsEmptyViewModelMessage;
                    if((dirtyFlags & 0x61L) != 0) {
                        if(TextUtilsIsEmptyViewModelMessage1) {
                                dirtyFlags |= 0x400L;
                        }
                        else {
                                dirtyFlags |= 0x200L;
                        }
                    }
                }
            }
            if ((dirtyFlags & 0x51L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.extraRequiredAdapter
                        viewModelExtraRequiredAdapter = viewModel.getExtraRequiredAdapter();
                    }
            }
            if ((dirtyFlags & 0x45L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.servicesRequiredAdapter
                        viewModelServicesRequiredAdapter = viewModel.getServicesRequiredAdapter();
                    }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x2000000000000L) != 0) {

                if (viewModel != null) {
                    // read viewModel.orderDetailsMain
                    viewModelOrderDetailsMain = viewModel.getOrderDetailsMain();
                }


                if (viewModelOrderDetailsMain != null) {
                    // read viewModel.orderDetailsMain.scheduledAt
                    viewModelOrderDetailsMainScheduledAt = viewModelOrderDetailsMain.getScheduledAt();
                }
        }
        if ((dirtyFlags & 0x100000000000L) != 0) {

                // read viewModel.orderDetailsMain.status != 4
                viewModelOrderDetailsMainStatusInt4 = (viewModelOrderDetailsMainStatus) != (4);
        }
        if ((dirtyFlags & 0x800L) != 0) {

                // read viewModel.orderDetailsMain.mainService.id == 19
                viewModelOrderDetailsMainMainServiceIdInt19 = (viewModelOrderDetailsMainMainServiceId) == (19);
            if((dirtyFlags & 0x800L) != 0) {
                if(viewModelOrderDetailsMainMainServiceIdInt19) {
                        dirtyFlags |= 0x100000000L;
                }
                else {
                        dirtyFlags |= 0x80000000L;
                }
            }


                // read viewModel.orderDetailsMain.mainService.id == 19 ? @android:string/license_civil_image_hint : @android:string/battery_image
                viewModelOrderDetailsMainMainServiceIdInt19TvBatteryAndroidStringLicenseCivilImageHintTvBatteryAndroidStringBatteryImage = ((viewModelOrderDetailsMainMainServiceIdInt19) ? (tvBattery.getResources().getString(R.string.license_civil_image_hint)) : (tvBattery.getResources().getString(R.string.battery_image)));
        }
        if ((dirtyFlags & 0x100L) != 0) {

                // read viewModel.orderDetailsMain.status <= 2
                viewModelOrderDetailsMainStatusInt2 = (viewModelOrderDetailsMainStatus) <= (2);
        }
        if ((dirtyFlags & 0x8000L) != 0) {

                // read viewModel.orderDetailsMain.status == 3
                viewModelOrderDetailsMainStatusInt3 = (viewModelOrderDetailsMainStatus) == (3);
        }
        if ((dirtyFlags & 0x400L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.SHOW_PROGRESS)
                    viewModelMessageEqualsConstantsSHOWPROGRESS = viewModelMessage.equals(grand.app.aber_provider.utils.Constants.SHOW_PROGRESS);
                }
        }
        if ((dirtyFlags & 0x20000000L) != 0) {

                if (viewModelMessage != null) {
                    // read viewModel.message.equals(Constants.HIDE_PROGRESS)
                    viewModelMessageEqualsConstantsHIDEPROGRESS = viewModelMessage.equals(grand.app.aber_provider.utils.Constants.HIDE_PROGRESS);
                }
        }

        if ((dirtyFlags & 0x43L) != 0) {

                // read viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status <= 2 : false
                viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalse = ((ViewModelOrderDetailsMainStatusInt01) ? (viewModelOrderDetailsMainStatusInt2) : (false));
                // read viewModel.orderDetailsMain.mainService.id == 9 ? @android:string/tier_image : viewModel.orderDetailsMain.mainService.id == 19 ? @android:string/license_civil_image_hint : @android:string/battery_image
                viewModelOrderDetailsMainMainServiceIdInt9TvBatteryAndroidStringTierImageViewModelOrderDetailsMainMainServiceIdInt19TvBatteryAndroidStringLicenseCivilImageHintTvBatteryAndroidStringBatteryImage = ((viewModelOrderDetailsMainMainServiceIdInt9) ? (tvBattery.getResources().getString(R.string.tier_image)) : (viewModelOrderDetailsMainMainServiceIdInt19TvBatteryAndroidStringLicenseCivilImageHintTvBatteryAndroidStringBatteryImage));
                // read viewModel.orderDetailsMain.status == 0 ? true : viewModel.orderDetailsMain.status == 3
                viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3 = ((viewModelOrderDetailsMainStatusInt0) ? (true) : (viewModelOrderDetailsMainStatusInt3));
                // read viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status != 4 : false
                viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalse = ((ViewModelOrderDetailsMainStatusInt01) ? (viewModelOrderDetailsMainStatusInt4) : (false));
                // read viewModel.orderDetailsMain.isEmergency == 1 ? @android:string/emergency_services : viewModel.orderDetailsMain.scheduledAt
                viewModelOrderDetailsMainIsEmergencyInt1MboundView10AndroidStringEmergencyServicesViewModelOrderDetailsMainScheduledAt = ((viewModelOrderDetailsMainIsEmergencyInt1) ? (mboundView10.getResources().getString(R.string.emergency_services)) : (viewModelOrderDetailsMainScheduledAt));
            if((dirtyFlags & 0x43L) != 0) {
                if(viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalse) {
                        dirtyFlags |= 0x10000000000L;
                }
                else {
                        dirtyFlags |= 0x8000000000L;
                }
            }
            if((dirtyFlags & 0x43L) != 0) {
                if(viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3) {
                        dirtyFlags |= 0x4000000000L;
                }
                else {
                        dirtyFlags |= 0x2000000000L;
                }
            }
            if((dirtyFlags & 0x43L) != 0) {
                if(viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalse) {
                        dirtyFlags |= 0x100000L;
                }
                else {
                        dirtyFlags |= 0x80000L;
                }
            }


                // read viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status <= 2 : false ? View.VISIBLE : View.GONE
                viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalseViewVISIBLEViewGONE = ((viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read viewModel.orderDetailsMain.status == 0 ? true : viewModel.orderDetailsMain.status == 3 ? View.VISIBLE : View.GONE
                viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3ViewVISIBLEViewGONE = ((viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status != 4 : false ? View.VISIBLE : View.GONE
                viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalseViewVISIBLEViewGONE = ((viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        if ((dirtyFlags & 0x61L) != 0) {

                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse = ((TextUtilsIsEmptyViewModelMessage1) ? (viewModelMessageEqualsConstantsSHOWPROGRESS) : (false));
            if((dirtyFlags & 0x61L) != 0) {
                if(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) {
                        dirtyFlags |= 0x40000L;
                }
                else {
                        dirtyFlags |= 0x20000L;
                }
            }


                // read !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
                textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE = ((textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        if ((dirtyFlags & 0x63L) != 0) {

                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS = ((textUtilsIsEmptyViewModelMessage) ? (true) : (viewModelMessageEqualsConstantsHIDEPROGRESS));
            if((dirtyFlags & 0x63L) != 0) {
                if(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) {
                        dirtyFlags |= 0x1000000000000L;
                }
                else {
                        dirtyFlags |= 0x800000000000L;
                }
            }
        }
        // batch finished

        if ((dirtyFlags & 0x1000000000000L) != 0) {

                if (viewModel != null) {
                    // read viewModel.orderDetailsMain
                    viewModelOrderDetailsMain = viewModel.getOrderDetailsMain();
                }


                if (viewModelOrderDetailsMain != null) {
                    // read viewModel.orderDetailsMain.canceled
                    viewModelOrderDetailsMainCanceled = viewModelOrderDetailsMain.getCanceled();
                }


                // read viewModel.orderDetailsMain.canceled != 1
                viewModelOrderDetailsMainCanceledInt1 = (viewModelOrderDetailsMainCanceled) != (1);
        }

        if ((dirtyFlags & 0x63L) != 0) {

                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? viewModel.orderDetailsMain.canceled != 1 : false
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalse = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESS) ? (viewModelOrderDetailsMainCanceledInt1) : (false));
            if((dirtyFlags & 0x63L) != 0) {
                if(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalse) {
                        dirtyFlags |= 0x400000000L;
                }
                else {
                        dirtyFlags |= 0x200000000L;
                }
            }


                // read TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? viewModel.orderDetailsMain.canceled != 1 : false ? true : false
                textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalseBooleanTrueBooleanFalse = ((textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalse) ? (true) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0x40L) != 0) {
            // api target 1

            this.btnAccept.setOnClickListener(mCallback94);
            this.btnReject.setOnClickListener(mCallback95);
            this.callBtn.setOnClickListener(mCallback92);
            this.chatBtn.setOnClickListener(mCallback91);
            this.locationImage.setOnClickListener(mCallback93);
            this.status.setOnClickListener(mCallback96);
        }
        if ((dirtyFlags & 0x43L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnAccept, viewModelOrderDetailsMainStatusInt0BtnAcceptAndroidStringAcceptBtnAcceptAndroidStringOrderFinished);
            this.btnAccept.setVisibility(viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3ViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnReject, viewModelOrderDetailsMainStatusInt0BtnRejectAndroidStringRejectBtnRejectAndroidStringNotDone);
            this.btnReject.setVisibility(viewModelOrderDetailsMainStatusInt0BooleanTrueViewModelOrderDetailsMainStatusInt3ViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.caYearValue, viewModelOrderDetailsMainVehicleSubChild);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.carCatValue, viewModelOrderDetailsMainVehicleChild);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.carTypeValue, viewModelOrderDetailsMainVehicleParent);
            this.cardBattery.setVisibility(textUtilsIsEmptyViewModelOrderDetailsMainImageViewVISIBLEViewGONE);
            this.cardCarDesc.setVisibility(viewModelOrderDetailsMainVehicleJavaLangObjectNullViewVISIBLEViewGONE);
            this.cardExtraService.setVisibility(viewModelOrderDetailsMainExtraServicesSizeInt0ViewGONEViewVISIBLE);
            this.cardMainService.setVisibility(viewModelOrderDetailsMainSubServicesSizeInt0ViewVISIBLEViewGONE);
            this.cardOtherService.setVisibility(viewModelOrderDetailsMainChildServicesSizeInt0ViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.createAt, viewModelOrderDetailsMainReviewCreatedAt);
            grand.app.aber_provider.base.ApplicationBinding.loadCommentImage(this.icProviderRate, viewModelOrderDetailsMainReviewUserName);
            grand.app.aber_provider.base.ApplicationBinding.loadImage(this.itemImg, viewModelOrderDetailsMainMainServiceImage);
            grand.app.aber_provider.base.ApplicationBinding.loadImage(this.locationImage, viewModelOrderDetailsMainStaticLocationImage);
            this.mboundView0.setVisibility(viewModelOrderDetailsMainUserJavaLangObjectNullViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView10, viewModelOrderDetailsMainIsEmergencyInt1MboundView10AndroidStringEmergencyServicesViewModelOrderDetailsMainScheduledAt);
            grand.app.aber_provider.base.ApplicationBinding.loadCommentImage(this.mboundView28, viewModelOrderDetailsMainImage);
            this.mboundView36.setVisibility(viewModelOrderDetailsMainReviewJavaLangObjectNullViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView41, viewModelOrderDetailsMainReviewComment);
            this.mboundView6.setVisibility(viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt4BooleanFalseViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView9, viewModelOrderDetailsMainMainServiceName);
            grand.app.aber_provider.base.ApplicationBinding.setRate(this.myRate, viewModelOrderDetailsMainReviewRate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.status, viewModelOrderDetailsMainStatusText);
            this.status.setVisibility(viewModelOrderDetailsMainStatusInt0ViewModelOrderDetailsMainStatusInt2BooleanFalseViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvBattery, viewModelOrderDetailsMainMainServiceIdInt9TvBatteryAndroidStringTierImageViewModelOrderDetailsMainMainServiceIdInt19TvBatteryAndroidStringLicenseCivilImageHintTvBatteryAndroidStringBatteryImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDeliveryPrice, stringValueOfViewModelOrderDetailsMainDeliveryFeesConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvEmePrice, stringValueOfViewModelOrderDetailsMainEmergencyCostConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHomeItem, viewModelOrderDetailsMainUserName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHomeJob, viewModelOrderDetailsMainAddress);
            this.tvMyReviewProvider.setVisibility(viewModelOrderDetailsMainReviewJavaLangObjectNullViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPrice, stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProviderName, viewModelOrderDetailsMainReviewUserName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServiceDescValue, viewModelOrderDetailsMainDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvServicesExtraPrice, stringValueOfViewModelOrderDetailsMainExtraFeesConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalPrice, stringValueOfViewModelOrderDetailsMainTotalConcatJavaLangStringConcatViewModelCurrency);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalServicesPrice, stringValueOfViewModelOrderDetailsMainSubtotalConcatJavaLangStringConcatViewModelCurrency);
            grand.app.aber_provider.base.ApplicationBinding.loadCommentImage(this.userProfileImg, viewModelOrderDetailsMainUserImage);
        }
        if ((dirtyFlags & 0x45L) != 0) {
            // api target 1

            grand.app.aber_provider.base.ApplicationBinding.getItemsV2Binding(this.mboundView12, viewModelServicesRequiredAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x49L) != 0) {
            // api target 1

            grand.app.aber_provider.base.ApplicationBinding.getItemsV2Binding(this.mboundView18, viewModelOptionsDetailsAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x51L) != 0) {
            // api target 1

            grand.app.aber_provider.base.ApplicationBinding.getItemsV2Binding(this.mboundView20, viewModelExtraRequiredAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x61L) != 0) {
            // api target 1

            this.progress.setVisibility(textUtilsIsEmptyViewModelMessageViewModelMessageEqualsConstantsSHOWPROGRESSBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x63L) != 0) {
            // api target 1

            this.status.setEnabled(textUtilsIsEmptyViewModelMessageBooleanTrueViewModelMessageEqualsConstantsHIDEPROGRESSViewModelOrderDetailsMainCanceledInt1BooleanFalseBooleanTrueBooleanFalse);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // viewModel.orderDetailsMain != null
                boolean viewModelOrderDetailsMainJavaLangObjectNull = false;
                // viewModel.orderDetailsMain.status == 0
                boolean viewModelOrderDetailsMainStatusInt0 = false;
                // viewModel.orderDetailsMain.status == 0 ? 1 : Constants.DONE
                int viewModelOrderDetailsMainStatusInt0Int1ConstantsDONE = 0;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;
                // viewModel.orderDetailsMain
                grand.app.aber_provider.pages.orderDetails.models.OrderDetailsMain viewModelOrderDetailsMain = null;
                // viewModel
                grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
                // viewModel.orderDetailsMain.status
                int viewModelOrderDetailsMainStatus = 0;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModelOrderDetailsMain = viewModel.getOrderDetailsMain();

                    viewModelOrderDetailsMainJavaLangObjectNull = (viewModelOrderDetailsMain) != (null);
                    if (viewModelOrderDetailsMainJavaLangObjectNull) {


                        viewModelOrderDetailsMainStatus = viewModelOrderDetailsMain.getStatus();


                        viewModelOrderDetailsMainStatusInt0 = (viewModelOrderDetailsMainStatus) == (0);
                        if (viewModelOrderDetailsMainStatusInt0) {



                            viewModelOrderDetailsMainStatusInt0Int1ConstantsDONE = 1;

                            viewModel.changeStatus(viewModelOrderDetailsMainStatusInt0Int1ConstantsDONE);
                        }
                        else {




                            viewModelOrderDetailsMainStatusInt0Int1ConstantsDONE = grand.app.aber_provider.utils.Constants.DONE;

                            viewModel.changeStatus(viewModelOrderDetailsMainStatusInt0Int1ConstantsDONE);
                        }
                    }
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // viewModel.orderDetailsMain != null
                boolean viewModelOrderDetailsMainJavaLangObjectNull = false;
                // viewModel.orderDetailsMain.status == 0
                boolean viewModelOrderDetailsMainStatusInt0 = false;
                // viewModel.orderDetailsMain.status == 0 ? -1 : Constants.NOT_DONE
                int viewModelOrderDetailsMainStatusInt0Int1ConstantsNOTDONE = 0;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;
                // viewModel.orderDetailsMain
                grand.app.aber_provider.pages.orderDetails.models.OrderDetailsMain viewModelOrderDetailsMain = null;
                // viewModel
                grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
                // viewModel.orderDetailsMain.status
                int viewModelOrderDetailsMainStatus = 0;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModelOrderDetailsMain = viewModel.getOrderDetailsMain();

                    viewModelOrderDetailsMainJavaLangObjectNull = (viewModelOrderDetailsMain) != (null);
                    if (viewModelOrderDetailsMainJavaLangObjectNull) {


                        viewModelOrderDetailsMainStatus = viewModelOrderDetailsMain.getStatus();


                        viewModelOrderDetailsMainStatusInt0 = (viewModelOrderDetailsMainStatus) == (0);
                        if (viewModelOrderDetailsMainStatusInt0) {




                            viewModelOrderDetailsMainStatusInt0Int1ConstantsNOTDONE = -1;

                            viewModel.changeStatus(viewModelOrderDetailsMainStatusInt0Int1ConstantsNOTDONE);
                        }
                        else {




                            viewModelOrderDetailsMainStatusInt0Int1ConstantsNOTDONE = grand.app.aber_provider.utils.Constants.NOT_DONE;

                            viewModel.changeStatus(viewModelOrderDetailsMainStatusInt0Int1ConstantsNOTDONE);
                        }
                    }
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.action(grand.app.aber_provider.utils.Constants.CALL);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.toFollowOrder();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModel.action(grand.app.aber_provider.utils.Constants.CHAT);
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // viewModel.orderDetailsMain != null
                boolean viewModelOrderDetailsMainJavaLangObjectNull = false;
                // (viewModel.orderDetailsMain.status) + (1)
                int viewModelOrderDetailsMainStatusInt1 = 0;
                // viewModel.orderDetailsMain
                grand.app.aber_provider.pages.orderDetails.models.OrderDetailsMain viewModelOrderDetailsMain = null;
                // viewModel
                grand.app.aber_provider.pages.orderDetails.viewModels.OrderDetailsViewModel viewModel = mViewModel;
                // viewModel.orderDetailsMain.status
                int viewModelOrderDetailsMainStatus = 0;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {




                    viewModelOrderDetailsMain = viewModel.getOrderDetailsMain();

                    viewModelOrderDetailsMainJavaLangObjectNull = (viewModelOrderDetailsMain) != (null);
                    if (viewModelOrderDetailsMainJavaLangObjectNull) {


                        viewModelOrderDetailsMainStatus = viewModelOrderDetailsMain.getStatus();


                        viewModelOrderDetailsMainStatusInt1 = (viewModelOrderDetailsMainStatus) + (1);

                        viewModel.changeStatus(viewModelOrderDetailsMainStatusInt1);
                    }
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.orderDetailsMain
        flag 2 (0x3L): viewModel.servicesRequiredAdapter
        flag 3 (0x4L): viewModel.optionsDetailsAdapter
        flag 4 (0x5L): viewModel.extraRequiredAdapter
        flag 5 (0x6L): viewModel.message
        flag 6 (0x7L): null
        flag 7 (0x8L): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status <= 2 : false
        flag 8 (0x9L): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status <= 2 : false
        flag 9 (0xaL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 10 (0xbL): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false
        flag 11 (0xcL): viewModel.orderDetailsMain.mainService.id == 9 ? @android:string/tier_image : viewModel.orderDetailsMain.mainService.id == 19 ? @android:string/license_civil_image_hint : @android:string/battery_image
        flag 12 (0xdL): viewModel.orderDetailsMain.mainService.id == 9 ? @android:string/tier_image : viewModel.orderDetailsMain.mainService.id == 19 ? @android:string/license_civil_image_hint : @android:string/battery_image
        flag 13 (0xeL): !TextUtils.isEmpty(viewModel.orderDetailsMain.image) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): !TextUtils.isEmpty(viewModel.orderDetailsMain.image) ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.orderDetailsMain.status == 0 ? true : viewModel.orderDetailsMain.status == 3
        flag 16 (0x11L): viewModel.orderDetailsMain.status == 0 ? true : viewModel.orderDetailsMain.status == 3
        flag 17 (0x12L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 18 (0x13L): !TextUtils.isEmpty(viewModel.message) ? viewModel.message.equals(Constants.SHOW_PROGRESS) : false ? View.VISIBLE : View.GONE
        flag 19 (0x14L): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status != 4 : false ? View.VISIBLE : View.GONE
        flag 20 (0x15L): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status != 4 : false ? View.VISIBLE : View.GONE
        flag 21 (0x16L): viewModel.orderDetailsMain.vehicle != null ? View.VISIBLE : View.GONE
        flag 22 (0x17L): viewModel.orderDetailsMain.vehicle != null ? View.VISIBLE : View.GONE
        flag 23 (0x18L): viewModel.orderDetailsMain.status == 0 ? @android:string/accept : @android:string/order_finished
        flag 24 (0x19L): viewModel.orderDetailsMain.status == 0 ? @android:string/accept : @android:string/order_finished
        flag 25 (0x1aL): viewModel.orderDetailsMain.subServices.size() > 0 ? View.VISIBLE : View.GONE
        flag 26 (0x1bL): viewModel.orderDetailsMain.subServices.size() > 0 ? View.VISIBLE : View.GONE
        flag 27 (0x1cL): viewModel.orderDetailsMain.review == null ? View.GONE : View.VISIBLE
        flag 28 (0x1dL): viewModel.orderDetailsMain.review == null ? View.GONE : View.VISIBLE
        flag 29 (0x1eL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 30 (0x1fL): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS)
        flag 31 (0x20L): viewModel.orderDetailsMain.mainService.id == 19 ? @android:string/license_civil_image_hint : @android:string/battery_image
        flag 32 (0x21L): viewModel.orderDetailsMain.mainService.id == 19 ? @android:string/license_civil_image_hint : @android:string/battery_image
        flag 33 (0x22L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? viewModel.orderDetailsMain.canceled != 1 : false ? true : false
        flag 34 (0x23L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? viewModel.orderDetailsMain.canceled != 1 : false ? true : false
        flag 35 (0x24L): viewModel.orderDetailsMain.status == 0 ? @android:string/reject : @android:string/not_done
        flag 36 (0x25L): viewModel.orderDetailsMain.status == 0 ? @android:string/reject : @android:string/not_done
        flag 37 (0x26L): viewModel.orderDetailsMain.status == 0 ? true : viewModel.orderDetailsMain.status == 3 ? View.VISIBLE : View.GONE
        flag 38 (0x27L): viewModel.orderDetailsMain.status == 0 ? true : viewModel.orderDetailsMain.status == 3 ? View.VISIBLE : View.GONE
        flag 39 (0x28L): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status <= 2 : false ? View.VISIBLE : View.GONE
        flag 40 (0x29L): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status <= 2 : false ? View.VISIBLE : View.GONE
        flag 41 (0x2aL): viewModel.orderDetailsMain.childServices.size() == 0 ? View.GONE : View.VISIBLE
        flag 42 (0x2bL): viewModel.orderDetailsMain.childServices.size() == 0 ? View.GONE : View.VISIBLE
        flag 43 (0x2cL): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status != 4 : false
        flag 44 (0x2dL): viewModel.orderDetailsMain.status != 0 ? viewModel.orderDetailsMain.status != 4 : false
        flag 45 (0x2eL): viewModel.orderDetailsMain.user != null ? View.VISIBLE : View.GONE
        flag 46 (0x2fL): viewModel.orderDetailsMain.user != null ? View.VISIBLE : View.GONE
        flag 47 (0x30L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? viewModel.orderDetailsMain.canceled != 1 : false
        flag 48 (0x31L): TextUtils.isEmpty(viewModel.message) ? true : viewModel.message.equals(Constants.HIDE_PROGRESS) ? viewModel.orderDetailsMain.canceled != 1 : false
        flag 49 (0x32L): viewModel.orderDetailsMain.isEmergency == 1 ? @android:string/emergency_services : viewModel.orderDetailsMain.scheduledAt
        flag 50 (0x33L): viewModel.orderDetailsMain.isEmergency == 1 ? @android:string/emergency_services : viewModel.orderDetailsMain.scheduledAt
        flag 51 (0x34L): viewModel.orderDetailsMain.extraServices.size() == 0 ? View.GONE : View.VISIBLE
        flag 52 (0x35L): viewModel.orderDetailsMain.extraServices.size() == 0 ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}