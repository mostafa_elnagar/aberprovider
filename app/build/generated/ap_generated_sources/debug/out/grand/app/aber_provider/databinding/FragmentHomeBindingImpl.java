package grand.app.aber_provider.databinding;
import grand.app.aber_provider.R;
import grand.app.aber_provider.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentHomeBindingImpl extends FragmentHomeBinding implements grand.app.aber_provider.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.searchContainer, 3);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback70;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentHomeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private FragmentHomeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.recyclerview.widget.RecyclerView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (com.google.android.material.switchmaterial.SwitchMaterial) bindings[1]
            );
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.rcPosts.setTag(null);
        this.tvSwitchMode.setTag(null);
        setRootTag(root);
        // listeners
        mCallback70 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((grand.app.aber_provider.pages.home.viewModels.HomeViewModels) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable grand.app.aber_provider.pages.home.viewModels.HomeViewModels ViewModel) {
        updateRegistration(0, ViewModel);
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModel((grand.app.aber_provider.pages.home.viewModels.HomeViewModels) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModel(grand.app.aber_provider.pages.home.viewModels.HomeViewModels ViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.orderAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelUserDataOnlineInt1TvSwitchModeAndroidStringTvSwitchModeTvSwitchModeAndroidStringTvSwitchModeOn = null;
        grand.app.aber_provider.pages.auth.models.UserData viewModelUserData = null;
        int viewModelUserDataOnline = 0;
        boolean viewModelUserDataOnlineInt1 = false;
        grand.app.aber_provider.pages.home.viewModels.HomeViewModels viewModel = mViewModel;
        grand.app.aber_provider.pages.home.adapters.OrderAdapter viewModelOrderAdapter = null;

        if ((dirtyFlags & 0x7L) != 0) {


            if ((dirtyFlags & 0x5L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.userData
                        viewModelUserData = viewModel.userData;
                    }


                    if (viewModelUserData != null) {
                        // read viewModel.userData.online
                        viewModelUserDataOnline = viewModelUserData.getOnline();
                    }


                    // read viewModel.userData.online == 1
                    viewModelUserDataOnlineInt1 = (viewModelUserDataOnline) == (1);
                if((dirtyFlags & 0x5L) != 0) {
                    if(viewModelUserDataOnlineInt1) {
                            dirtyFlags |= 0x10L;
                    }
                    else {
                            dirtyFlags |= 0x8L;
                    }
                }


                    // read viewModel.userData.online == 1 ? @android:string/tv_switch_mode : @android:string/tv_switch_mode_on
                    viewModelUserDataOnlineInt1TvSwitchModeAndroidStringTvSwitchModeTvSwitchModeAndroidStringTvSwitchModeOn = ((viewModelUserDataOnlineInt1) ? (tvSwitchMode.getResources().getString(R.string.tv_switch_mode)) : (tvSwitchMode.getResources().getString(R.string.tv_switch_mode_on)));
            }

                if (viewModel != null) {
                    // read viewModel.orderAdapter
                    viewModelOrderAdapter = viewModel.getOrderAdapter();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            grand.app.aber_provider.base.ApplicationBinding.getItemsV2Binding(this.rcPosts, viewModelOrderAdapter, "1", "1");
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.tvSwitchMode.setOnClickListener(mCallback70);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSwitchMode, viewModelUserDataOnlineInt1TvSwitchModeAndroidStringTvSwitchModeTvSwitchModeAndroidStringTvSwitchModeOn);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        grand.app.aber_provider.pages.home.viewModels.HomeViewModels viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.changeOnlineStatus();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): viewModel.orderAdapter
        flag 2 (0x3L): null
        flag 3 (0x4L): viewModel.userData.online == 1 ? @android:string/tv_switch_mode : @android:string/tv_switch_mode_on
        flag 4 (0x5L): viewModel.userData.online == 1 ? @android:string/tv_switch_mode : @android:string/tv_switch_mode_on
    flag mapping end*/
    //end
}