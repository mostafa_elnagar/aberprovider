package grand.app.aber_provider.databinding;
import grand.app.aber_provider.R;
import grand.app.aber_provider.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class MenuBindingImpl extends MenuBinding implements grand.app.aber_provider.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.v1, 12);
        sViewsWithIds.put(R.id.v5, 13);
        sViewsWithIds.put(R.id.v7, 14);
        sViewsWithIds.put(R.id.v8, 15);
        sViewsWithIds.put(R.id.v9, 16);
        sViewsWithIds.put(R.id.v16, 17);
        sViewsWithIds.put(R.id.v17, 18);
        sViewsWithIds.put(R.id.v15, 19);
        sViewsWithIds.put(R.id.v20, 20);
        sViewsWithIds.put(R.id.v21, 21);
        sViewsWithIds.put(R.id.grand_logo, 22);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback49;
    @Nullable
    private final android.view.View.OnClickListener mCallback57;
    @Nullable
    private final android.view.View.OnClickListener mCallback55;
    @Nullable
    private final android.view.View.OnClickListener mCallback52;
    @Nullable
    private final android.view.View.OnClickListener mCallback50;
    @Nullable
    private final android.view.View.OnClickListener mCallback58;
    @Nullable
    private final android.view.View.OnClickListener mCallback56;
    @Nullable
    private final android.view.View.OnClickListener mCallback54;
    @Nullable
    private final android.view.View.OnClickListener mCallback53;
    @Nullable
    private final android.view.View.OnClickListener mCallback51;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public MenuBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 23, sIncludes, sViewsWithIds));
    }
    private MenuBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (grand.app.aber_provider.customViews.grandDialog.GrandImageDialog) bindings[22]
            , (androidx.recyclerview.widget.RecyclerView) bindings[11]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[8]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[7]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[1]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[6]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[10]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[9]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[2]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[4]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[5]
            , (grand.app.aber_provider.customViews.views.CustomTextViewMedium) bindings[3]
            , (android.view.View) bindings[12]
            , (android.view.View) bindings[19]
            , (android.view.View) bindings[17]
            , (android.view.View) bindings[18]
            , (android.view.View) bindings[20]
            , (android.view.View) bindings[21]
            , (android.view.View) bindings[13]
            , (android.view.View) bindings[14]
            , (android.view.View) bindings[15]
            , (android.view.View) bindings[16]
            );
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.rcMenuSocial.setTag(null);
        this.tvCity.setTag(null);
        this.tvCountry.setTag(null);
        this.tvHome.setTag(null);
        this.tvLang.setTag(null);
        this.tvLogout.setTag(null);
        this.tvMore.setTag(null);
        this.tvMyServices.setTag(null);
        this.tvNotifications.setTag(null);
        this.tvPrivacy.setTag(null);
        this.tvProvider.setTag(null);
        setRootTag(root);
        // listeners
        mCallback49 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 1);
        mCallback57 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 9);
        mCallback55 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 7);
        mCallback52 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 4);
        mCallback50 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 2);
        mCallback58 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 10);
        mCallback56 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 8);
        mCallback54 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 6);
        mCallback53 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 5);
        mCallback51 = new grand.app.aber_provider.generated.callback.OnClickListener(this, 3);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.menuViewModel == variableId) {
            setMenuViewModel((grand.app.aber_provider.customViews.views.MenuViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setMenuViewModel(@Nullable grand.app.aber_provider.customViews.views.MenuViewModel MenuViewModel) {
        updateRegistration(0, MenuViewModel);
        this.mMenuViewModel = MenuViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.menuViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeMenuViewModel((grand.app.aber_provider.customViews.views.MenuViewModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeMenuViewModel(grand.app.aber_provider.customViews.views.MenuViewModel MenuViewModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.socialAdapter) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
        grand.app.aber_provider.pages.settings.adapters.MenuSocialAdapter menuViewModelSocialAdapter = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (menuViewModel != null) {
                    // read menuViewModel.socialAdapter
                    menuViewModelSocialAdapter = menuViewModel.getSocialAdapter();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            grand.app.aber_provider.base.ApplicationBinding.getItemsV2Binding(this.rcMenuSocial, menuViewModelSocialAdapter, "1", "2");
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.tvCity.setOnClickListener(mCallback56);
            this.tvCountry.setOnClickListener(mCallback55);
            this.tvHome.setOnClickListener(mCallback49);
            this.tvLang.setOnClickListener(mCallback54);
            this.tvLogout.setOnClickListener(mCallback58);
            this.tvMore.setOnClickListener(mCallback57);
            this.tvMyServices.setOnClickListener(mCallback50);
            this.tvNotifications.setOnClickListener(mCallback52);
            this.tvPrivacy.setOnClickListener(mCallback53);
            this.tvProvider.setOnClickListener(mCallback51);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.MENU_HOME);
                }
                break;
            }
            case 9: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.MORE);
                }
                break;
            }
            case 7: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.COUNTRIES);
                }
                break;
            }
            case 4: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.NOTIFICATIONS);
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.MY_ORDERS);
                }
                break;
            }
            case 10: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.SHOW_LOGOUT_WARNING);
                }
                break;
            }
            case 8: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.CITIES);
                }
                break;
            }
            case 6: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.LANGUAGE);
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.MENU_ACCOUNT);
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // menuViewModel
                grand.app.aber_provider.customViews.views.MenuViewModel menuViewModel = mMenuViewModel;
                // menuViewModel != null
                boolean menuViewModelJavaLangObjectNull = false;



                menuViewModelJavaLangObjectNull = (menuViewModel) != (null);
                if (menuViewModelJavaLangObjectNull) {




                    menuViewModel.liveDataActions(grand.app.aber_provider.utils.Constants.PREVIOUS);
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): menuViewModel
        flag 1 (0x2L): menuViewModel.socialAdapter
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}