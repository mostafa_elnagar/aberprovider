// Generated by Dagger (https://dagger.dev).
package grand.app.aber_provider.pages.myOrders;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import grand.app.aber_provider.pages.myOrders.viewModels.MyOrdersViewModels;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MyServicesOrdersFragment_MembersInjector implements MembersInjector<MyServicesOrdersFragment> {
  private final Provider<MyOrdersViewModels> viewModelProvider;

  public MyServicesOrdersFragment_MembersInjector(Provider<MyOrdersViewModels> viewModelProvider) {
    this.viewModelProvider = viewModelProvider;
  }

  public static MembersInjector<MyServicesOrdersFragment> create(
      Provider<MyOrdersViewModels> viewModelProvider) {
    return new MyServicesOrdersFragment_MembersInjector(viewModelProvider);
  }

  @Override
  public void injectMembers(MyServicesOrdersFragment instance) {
    injectViewModel(instance, viewModelProvider.get());
  }

  @InjectedFieldSignature("grand.app.aber_provider.pages.myOrders.MyServicesOrdersFragment.viewModel")
  public static void injectViewModel(MyServicesOrdersFragment instance,
      MyOrdersViewModels viewModel) {
    instance.viewModel = viewModel;
  }
}
