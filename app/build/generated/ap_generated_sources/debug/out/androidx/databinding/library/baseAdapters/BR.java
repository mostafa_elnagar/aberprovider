package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int aboutData = 1;

  public static final int adapter = 2;

  public static final int baseViewModel = 3;

  public static final int chat = 4;

  public static final int childServices = 5;

  public static final int contact = 6;

  public static final int contactsAdapter = 7;

  public static final int conversationsAdapter = 8;

  public static final int conversationsData = 9;

  public static final int conversationsMain = 10;

  public static final int countriesAdapter = 11;

  public static final int countriesData = 12;

  public static final int extraRequiredAdapter = 13;

  public static final int historyWalletData = 14;

  public static final int itemChatViewModel = 15;

  public static final int itemPostViewModel = 16;

  public static final int itemViewModel = 17;

  public static final int itemWalletViewModel = 18;

  public static final int mainData = 19;

  public static final int mapAddressViewModel = 20;

  public static final int menuViewModel = 21;

  public static final int message = 22;

  public static final int myOrdersMain = 23;

  public static final int notificationsAdapter = 24;

  public static final int notificationsData = 25;

  public static final int notifyItemViewModels = 26;

  public static final int notifyViewModel = 27;

  public static final int onBoardAdapter = 28;

  public static final int onBoardViewModels = 29;

  public static final int optionsDetailsAdapter = 30;

  public static final int orderAdapter = 31;

  public static final int orderDetailsMain = 32;

  public static final int orderStatusRequest = 33;

  public static final int orders = 34;

  public static final int packagesAdapter = 35;

  public static final int packagesData = 36;

  public static final int passingObject = 37;

  public static final int ratesItem = 38;

  public static final int request = 39;

  public static final int reviewsAdapter = 40;

  public static final int reviewsMain = 41;

  public static final int service = 42;

  public static final int servicesAdapter = 43;

  public static final int servicesRequiredAdapter = 44;

  public static final int socialAdapter = 45;

  public static final int socialMediaData = 46;

  public static final int viewModel = 47;

  public static final int viewmodel = 48;

  public static final int walletHistoryItem = 49;
}
